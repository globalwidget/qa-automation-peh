package TestCases;

import java.lang.reflect.Method;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import scenarios.Learn.LabTests;
import scenarios.becomeDistributor.*;
import scenarios.cart.Cart;
import scenarios.checkout.Checkout;
import scenarios.contact.Contact;
import scenarios.headerAndFooter.HeaderAndFooters;
//import scenarios.headerAndFooter.HeaderAndFooters;
import scenarios.newsletter.Newsletter;
import automationFramework.GenericKeywords;
import baseClass.BaseClass;

@Listeners({ utilities.HtmlReport.class })
public class TestCases {
	private BaseClass obj;

	////////////////////////////////////////////////////////////////////////////////
	// Function Name :
	// Purpose :
	// Parameters :
	// Return Value :
	// Created by :
	// Created on :
	// Remarks :
	/////////////////////////////////////////////////////////////////////////////////

	private void TestStart(String testCaseName) {

		obj.currentTestCaseName = testCaseName;
		obj.setEnvironmentTimeouts();
		obj.reportStart();
		obj.iterationCount.clear();
		obj.iterationCountInTextData();

	}

	@BeforeClass
	@Parameters({ "selenium.machinename", "selenium.host", "selenium.port", "selenium.browser", "selenium.os",
			"selenium.browserVersion", "selenium.osVersion", "TestData.SheetNumber" })
	public void precondition(String machineName, String host, String port, String browser, String os,
			String browserVersion, String osVersion, String sheetNo) {
		obj = new BaseClass();
		if (os.contains("Android")) {
			obj.startServer(host, port);
			obj.waitTime(10);
		}
		obj.setup(machineName, host, port, browser, os, browserVersion, osVersion, sheetNo);
	}

	@Test(alwaysRun = true)
	public void Login_PPH_TS_005(Method method) {
		TestStart(method.getName());
		scenarios.login_logout.Login_Logout login_logout = new scenarios.login_logout.Login_Logout(obj);
		login_logout.dataRowNo = Integer.parseInt(login_logout.iterationCount.get(0).toString());
		login_logout.login_invalidCredentials();

		obj.testFailure = login_logout.testFailure;
		TestEnd();
	}

	@Test(alwaysRun = true)
	public void Login_PPH_TS_004(Method method) {
		TestStart(method.getName());
		scenarios.login_logout.Login_Logout login_logout = new scenarios.login_logout.Login_Logout(obj);
		login_logout.dataRowNo = Integer.parseInt(login_logout.iterationCount.get(0).toString());
		login_logout.login();

		obj.testFailure = login_logout.testFailure;
		TestEnd();

	}

	@Test(alwaysRun = true)
	public void Logout_PPH_TS_007(Method method) {
		TestStart(method.getName());
		scenarios.login_logout.Login_Logout login_logout = new scenarios.login_logout.Login_Logout(obj);
		login_logout.dataRowNo = Integer.parseInt(login_logout.iterationCount.get(0).toString());
		login_logout.logout();

		obj.testFailure = login_logout.testFailure;
		TestEnd();

	}

	@Test(alwaysRun = true)
	public void Register_PPH_TS_001(Method method) {
		TestStart(method.getName());
		scenarios.register.Register register = new scenarios.register.Register(obj);
		register.dataRowNo = Integer.parseInt(register.iterationCount.get(0).toString());
		register.register();
		obj.testFailure = register.testFailure;
		TestEnd();

	}

	@Test(alwaysRun = true)
	public void Register_PPH_TS_002(Method method) {
		TestStart(method.getName());
		scenarios.register.Register register = new scenarios.register.Register(obj);
		register.dataRowNo = Integer.parseInt(register.iterationCount.get(0).toString());
		register.register_InvalidCredentials();
		register.register_existingCredentials();
		obj.testFailure = register.testFailure;
		TestEnd();

	}

	@Test(alwaysRun = true)
	public void BecomeDistributor_PPH_TS_030(Method method) {
		TestStart(method.getName());
		BecomeDistributor wholesale = new BecomeDistributor(obj);
		wholesale.dataRowNo = Integer.parseInt(wholesale.iterationCount.get(0).toString());
		wholesale.wholesale_submitWithValidDetails();
		obj.testFailure = wholesale.testFailure;
		TestEnd();

	}

	@Test(alwaysRun = true)
	public void BecomeDistributor_PPH_TS_031(Method method) {
		TestStart(method.getName());
		BecomeDistributor wholesale = new BecomeDistributor(obj);
		wholesale.dataRowNo = Integer.parseInt(wholesale.iterationCount.get(0).toString());
		wholesale.wholesale_submitWithInvalidDetails();
		obj.testFailure = wholesale.testFailure;
		TestEnd();

	}

	@Test(alwaysRun = true)
	public void PGP_PPH_TS_014(Method method) {
		TestStart(method.getName());
		Cart cart = new Cart(obj);
		cart.dataRowNo = Integer.parseInt(cart.iterationCount.get(0).toString());
		cart.addProductToCart(1, true);
		obj.testFailure = cart.testFailure;
		TestEnd();

	}

	@Test(alwaysRun = true)
	public void Cart_PPH_TS_043(Method method) {
		TestStart(method.getName());
		Cart cart = new Cart(obj);
		cart.dataRowNo = Integer.parseInt(cart.iterationCount.get(0).toString());
		cart.addProductToCart(1, false);
		obj.testFailure = cart.testFailure;
		TestEnd();

	}

	@Test(alwaysRun = true)
	public void Cart_PPH_TS_044(Method method) {
		TestStart(method.getName());
		Cart cart = new Cart(obj);
		cart.dataRowNo = Integer.parseInt(cart.iterationCount.get(0).toString());
		cart.verifyRemoveProductFromCart();
		obj.testFailure = cart.testFailure;
		TestEnd();

	}

	@Test(alwaysRun = true)
	public void Cart_PPH_TS_045(Method method) {
		TestStart(method.getName());
		Cart cart = new Cart(obj);
		cart.dataRowNo = Integer.parseInt(cart.iterationCount.get(0).toString());
		cart.verifyUpdateQuantityInCart();
		obj.testFailure = cart.testFailure;
		TestEnd();

	}

	@Test(alwaysRun = true)
	public void Cart_PPH_TS_046(Method method) {
		TestStart(method.getName());
		Cart cart = new Cart(obj);
		cart.dataRowNo = Integer.parseInt(cart.iterationCount.get(0).toString());
		cart.verifyValidCouponEntry();
		obj.testFailure = cart.testFailure;
		TestEnd();

	}

	@Test(alwaysRun = true)
	public void Cart_PPH_TS_047(Method method) {
		TestStart(method.getName());
		Cart cart = new Cart(obj);
		cart.dataRowNo = Integer.parseInt(cart.iterationCount.get(0).toString());
		cart.verifyInvalidCouponEntry();
		obj.testFailure = cart.testFailure;
		TestEnd();

	}

	@Test(alwaysRun = true)
	public void PDP_PPH_TS_025(Method method) {
		TestStart(method.getName());
		Cart cart = new Cart(obj);
		cart.dataRowNo = Integer.parseInt(cart.iterationCount.get(0).toString());
		cart.verifyUpdateQuantity_AddToCart();
		obj.testFailure = cart.testFailure;
		TestEnd();

	}

	@Test(alwaysRun = true)
	public void Cart_PPH_TS_087(Method method) {
		TestStart(method.getName());
		Cart cart = new Cart(obj);
		cart.dataRowNo = Integer.parseInt(cart.iterationCount.get(0).toString());
		cart.addProductToCart(2, false);
		obj.testFailure = cart.testFailure;
		TestEnd();

	}

	@Test(alwaysRun = true)
	public void Checkout_PPH_TS_067(Method method) {
		TestStart(method.getName());
		Checkout checkout = new Checkout(obj);
		checkout.dataRowNo = Integer.parseInt(checkout.iterationCount.get(0).toString());
		checkout.checkout_InvalidDetails();
		obj.testFailure = checkout.testFailure;
		TestEnd();

	}

	@Test(alwaysRun = true)
	public void Checkout_PPH_TS_064(Method method) {
		TestStart(method.getName());
		Checkout checkout = new Checkout(obj);
		checkout.dataRowNo = Integer.parseInt(checkout.iterationCount.get(0).toString());
		checkout.checkout_InvalidCreditCardDetails();
		obj.testFailure = checkout.testFailure;
		TestEnd();

	}

	@Test(alwaysRun = true)
	public void Checkout_PPH_TS_063_066(Method method) {
		TestStart(method.getName());
		Checkout checkout = new Checkout(obj);
		checkout.dataRowNo = Integer.parseInt(checkout.iterationCount.get(0).toString());
		checkout.checkout_ValidDetails();
		obj.testFailure = checkout.testFailure;
		TestEnd();

	}

	@Test(alwaysRun = true)
	public void Checkout_PPH_TS_050(Method method) {
		TestStart(method.getName());
		Checkout checkout = new Checkout(obj);
		checkout.dataRowNo = Integer.parseInt(checkout.iterationCount.get(0).toString());
		checkout.checkout_EditCheckoutFormGuestUser();
		obj.testFailure = checkout.testFailure;
		TestEnd();

	}

	@Test(alwaysRun = true)
	public void Checkout_PPH_TS_061(Method method) {
		TestStart(method.getName());
		Checkout checkout = new Checkout(obj);
		checkout.dataRowNo = Integer.parseInt(checkout.iterationCount.get(0).toString());
		checkout.checkout_CompleteCheckoutGuestUser();
		obj.testFailure = checkout.testFailure;
		TestEnd();

	}

	@Test(alwaysRun = true)
	public void Checkout_PPH_TS_051(Method method) {
		TestStart(method.getName());
		Checkout checkout = new Checkout(obj);
		checkout.dataRowNo = Integer.parseInt(checkout.iterationCount.get(0).toString());
		checkout.checkout_VerifySavedDetails();
		obj.testFailure = checkout.testFailure;
		TestEnd();

	}

	@Test(alwaysRun = true)
	public void Checkout_PPH_TS_052_062(Method method) {
		TestStart(method.getName());
		Checkout checkout = new Checkout(obj);
		checkout.dataRowNo = Integer.parseInt(checkout.iterationCount.get(0).toString());
		checkout.checkout_ShipDifferentAddress();
		obj.testFailure = checkout.testFailure;
		TestEnd();

	}

	@Test(alwaysRun = true)
	public void HeaderFooter_PPH_TS_085(Method method) {
		TestStart(method.getName());
		HeaderAndFooters headersAndFooters = new HeaderAndFooters(obj);
		headersAndFooters.dataRowNo = Integer.parseInt(headersAndFooters.iterationCount.get(0).toString());
		headersAndFooters.verifyHeader();
		obj.testFailure = headersAndFooters.testFailure;
		TestEnd();
	}

	@Test(alwaysRun = true)
	public void HeaderFooter_PPH_TS_086(Method method) {
		TestStart(method.getName());
		HeaderAndFooters headersAndFooters = new HeaderAndFooters(obj);
		headersAndFooters.dataRowNo = Integer.parseInt(headersAndFooters.iterationCount.get(0).toString());
		headersAndFooters.verifyFooter();
		obj.testFailure = headersAndFooters.testFailure;
		TestEnd();
	}

	@Test(alwaysRun = true)
	public void ContactUs_PPH_TS_034(Method method) {
		TestStart(method.getName());
		Contact contact = new Contact(obj);
		contact.dataRowNo = Integer.parseInt(contact.iterationCount.get(0).toString());
		contact.submitInvalidDetails();
		obj.testFailure = contact.testFailure;
		TestEnd();

	}

	@Test(alwaysRun = true)
	public void ContactUs_PPH_TS_033(Method method) {
		TestStart(method.getName());
		Contact contact = new Contact(obj);
		contact.dataRowNo = Integer.parseInt(contact.iterationCount.get(0).toString());
		contact.submitValidDetails();
		obj.testFailure = contact.testFailure;
		TestEnd();

	}

	@Test(alwaysRun = true)
	public void Newsletter_PPH_TS_084(Method method) {
		TestStart(method.getName());
		Newsletter newsletter = new Newsletter(obj);
		newsletter.dataRowNo = Integer.parseInt(newsletter.iterationCount.get(0).toString());
		newsletter.newsletter_invalidData();
		obj.testFailure = newsletter.testFailure;
		TestEnd();

	}

	@Test(alwaysRun = true)
	public void Newsletter_PPH_TS_083(Method method) {
		TestStart(method.getName());
		Newsletter newsletter = new Newsletter(obj);
		newsletter.dataRowNo = Integer.parseInt(newsletter.iterationCount.get(0).toString());
		newsletter.newsletter_validData();
		obj.testFailure = newsletter.testFailure;
		TestEnd();

	}

	@Test(alwaysRun = true)
	public void Learn_PPH_TS_039(Method method) {
		TestStart(method.getName());
		LabTests labTests = new LabTests(obj);
		labTests.dataRowNo = Integer.parseInt(labTests.iterationCount.get(0).toString());
		labTests.verifyInvalidSearch();
		labTests.verifyValidSearch();
		obj.testFailure = labTests.testFailure;
		TestEnd();
	}

	@AfterClass
	public void closeSessions() {
		obj.closeAllSessions();
	}

	private void TestEnd() {
		obj.waitTime(1);
		if (obj.testFailure) {
			GenericKeywords.testFailed();
		}
	}

	@BeforeMethod
	public void before() {
		obj.testFailure = false;
	}

}
