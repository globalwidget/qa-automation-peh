package pages;

import java.util.LinkedHashMap;
import java.util.Map;

import baseClass.BaseClass;
import iSAFE.ApplicationKeywords;

public class BecomeDistributor extends ApplicationKeywords {

	private static final String firstName = "First Name #id=830553_45631pi_830553_45631";
	private static final String lastName = "Last Name #id=830553_45633pi_830553_45633";
	private static final String businessName = "Business Name #id=830553_45635pi_830553_45635";
	private static final String businessType = "Business Type #id=830553_45637pi_830553_45637";
	private static final String ein = "EIN #id=830553_45643pi_830553_45643";
	private static final String email = "Email #id=830553_45641pi_830553_45641";
	private static final String phone = "Phone #id=830553_45645pi_830553_45645";
	private static final String address = "Address #id=830553_45647pi_830553_45647";
	private static final String city = "City #id=830553_45639pi_830553_45639";
	private static final String state = "State #id=830553_45651pi_830553_45651";
	private static final String zip = "Zip #id=830553_45653pi_830553_45653";
	private static final String message = "Message #id=830553_45655pi_830553_45655";
	private static final String tellMeMore = "Tell Me More Button #xpath=//p[contains(@class,'submit')]/input";
	private static final String successMessage = "Success Message #xpath=//h1[contains(text(),'SUCCESS')]";
	private static final String firstNameMissingError = "First Name Missing Error #xpath=//p[contains(@class,'first_name')]//following-sibling::p[contains(text(),'This field is required')][1]";
	private static final String lastNameMissingError = "Last Name Missing Error #xpath=//p[contains(@class,'first_name')]//following-sibling::p[contains(text(),'This field is required')][2]";
	private static final String businessNameMissingError = "Business Name Missing Error #xpath=//p[contains(@class,'first_name')]//following-sibling::p[contains(text(),'This field is required')][3]";
	private static final String businessTypeMissingError = "Business Type Missing Error #xpath=//p[contains(@class,'first_name')]//following-sibling::p[contains(text(),'This field is required')][4]";
	private static final String einMissingError = "EIN Missing Error #xpath=//p[contains(@class,'first_name')]//following-sibling::p[contains(text(),'This field is required')][5]";
	private static final String emailInvalidError = "Email Missing Error #xpath=//p[contains(text(),'enter a valid email')]";
	private static final String phoneMissingError = "Phone Missing Error #xpath=//p[contains(@class,'first_name')]//following-sibling::p[contains(text(),'This field is required')][6]";
	private static final String addressMissingError = "Address Missing Error #xpath=//p[contains(@class,'first_name')]//following-sibling::p[contains(text(),'This field is required')][7]";
	private static final String cityMissingError = "City Missing Error #xpath=//p[contains(@class,'first_name')]//following-sibling::p[contains(text(),'This field is required')][8]";
	private static final String stateMissingError = "State Missing Error #xpath=//p[contains(@class,'first_name')]//following-sibling::p[contains(text(),'This field is required')][9]";
	private static final String zipMissingError = "Zip Missing Error #xpath=//p[contains(@class,'first_name')]//following-sibling::p[contains(text(),'This field is required')][10]";
	private static final String messageMissingError = "Message Missing Error #xpath=//p[contains(@class,'first_name')]//following-sibling::p[contains(text(),'This field is required')][11]";
	private static final String invalidCaptchaError = "Invalid Captcha Error #xpath=//p[contains(text(),'Invalid CAPTCHA')]";
	private static final String formIframe = "Form i-Frame #xpath=//div[contains(@class,'textwidget')]//iframe";

	public BecomeDistributor(BaseClass obj) {
		super(obj);
	}

	/**
	 * Description: Click on Submit button.
	 */
	public void clickTellMeMore() {
		try {
			switchToFrame(formIframe);
			if (isElementPresent(tellMeMore)) {
				scrollToViewElement(tellMeMore);
				highLighterMethod(tellMeMore);
				clickOn(tellMeMore);
			} else {
				testStepFailed("Could not click on the Tell Me More button", "Element not present");
			}
		} catch (Exception e) {
			testStepFailed("Could not click on the Tell Me More button");
			e.printStackTrace();
		}
		switchToDefaultFrame();
	}

	/**
	 * Description: Verify the Error Messages for all fields, when empty and clicked
	 * on send.
	 */
	public void VerifyErrorMessage_EmptyFields() {
		try {
			switchToFrame(formIframe);
			String[] Allfields = { firstNameMissingError, lastNameMissingError, businessNameMissingError,
					businessTypeMissingError, einMissingError, emailInvalidError, phoneMissingError,
					addressMissingError, cityMissingError, stateMissingError, zipMissingError, messageMissingError,
					invalidCaptchaError };
			for (String field : Allfields) {
				if (isElementPresent(field)) {
					if (isElementDisplayed(field)) {
						highLighterMethod(field);
						testStepInfo("Error Message is dispalyed when " + field.split("Missing Error #xpath")[0]
								+ " field is Empty");
					} else {
						testStepFailed("Error Message is not dispalyed when " + field.split("Missing Error #xpath")[0]
								+ "  field is Empty");
					}
				}
			}
		} catch (Exception e) {
			testStepFailed("Could not validate error messages for empty fields");
			e.printStackTrace();
		}
		switchToDefaultFrame();
	}

	/**
	 * Description: Verify the Error Messages when email entered is invalid and send
	 * is clicked
	 */
	public void VerifyErrorMessage_InvalidEmail() {
		try {
			switchToFrame(formIframe);
			waitForElementToDisplay(emailInvalidError, 10);
			if (isElementDisplayed(emailInvalidError)) {
				highLighterMethod(emailInvalidError);
				scrollToViewElement(emailInvalidError);
				testStepInfo("Error Message is dispalyed for invalid email");
			} else {
				testStepFailed("Error Message is not dispalyed for invalid email");
			}
		} catch (Exception e) {
			testStepFailed("Could not validate error message for invalid email");
			e.printStackTrace();
		}
		switchToDefaultFrame();
	}

	/**
	 * Description: Verify the Error Messages when email entered is invalid and send
	 * is clicked
	 * 
	 */
	public void FillAllDetails(String firstName, String lastName, String businessName, int businessType, String ein,
			String email, String phone, String address, String city, String state, String zip, String message) {
		try {
			Map<String, String> data_Details = new LinkedHashMap<String, String>(14);
			data_Details.put(firstName, BecomeDistributor.firstName);
			data_Details.put(lastName, BecomeDistributor.lastName);
			data_Details.put(businessName, BecomeDistributor.businessName);
			data_Details.put(ein, BecomeDistributor.ein);
			data_Details.put(email, BecomeDistributor.email);
			data_Details.put(phone, BecomeDistributor.phone);
			data_Details.put(address, BecomeDistributor.address);
			switchToFrame(formIframe);
			if (city.isEmpty() != true) {
				highLighterMethod(BecomeDistributor.city);
				typeIn(BecomeDistributor.city, city);
			}
			if (state.isEmpty() != true) {
				highLighterMethod(BecomeDistributor.state);
				typeIn(BecomeDistributor.state, state);
			}
			data_Details.put(zip, BecomeDistributor.zip);
			data_Details.put(message, BecomeDistributor.message);
			for (Map.Entry<String, String> entry : data_Details.entrySet()) {
				if (isElementDisplayed(entry.getValue())) {

					if (entry.getKey().isEmpty())
						continue;
					else {
						highLighterMethod(entry.getValue());
						typeIn(entry.getValue(), entry.getKey());
					}

				} else {
					testStepFailed(entry.getValue() + " field was not found");
				}
			}

			if (isElementDisplayed(BecomeDistributor.businessType)) {
				highLighterMethod(BecomeDistributor.businessType);
				selectFromDropdown(BecomeDistributor.businessType, businessType);
			} else {
				testStepFailed("Business Type field was not found");
			}
		} catch (Exception e) {
			e.printStackTrace();
			testStepFailed("Could not enter details for Distributor form");
		}
		switchToDefaultFrame();
	}

	/**
	 * Description: Verify the presence of Success Message displayed after valid
	 * details are submitted
	 */
	public void VerifySucessMessage() {
		try {
			waitForElementToDisplay(successMessage, 8);
			if (isElementDisplayed(successMessage)) {
				highLighterMethod(successMessage);
				testStepInfo("Success Message is dispalyed for sucessful submission of details");
			} else {
				testStepFailed("Success Message is not dispalyed for sucessful submission of details");
			}
		} catch (Exception e) {
			testStepFailed("Success Message could not be validated");
			e.printStackTrace();
		}
	}
}
