package pages;

import baseClass.BaseClass;
import iSAFE.ApplicationKeywords;

public class HeaderAndFooters extends ApplicationKeywords {

	// Header
	private static final String phoneNumber = "Phone Number #xpath=//b[contains(text(),'1-800-508-5448')]";
	private static final String customerCare = "Phone Number #xpath=//b[contains(text(),'Click')]";
	private static final String becomeDistributor = "Become Distributor #xpath=//b[contains(text(),'BECOME A DISTRIBUTOR')]";
	private static final String cart = "Cart #xpath=//div[@id='header-links']//a[contains(text(),'Cart')]";
	private static final String checkout = "Checkout #xpath=//div[@id='header-links']//a[contains(text(),'Checkout')]";
	private static final String myAccount = "Account #xpath=//div[@id='header-links']//a[contains(text(),'Account')]";
	private static final String home = "Home #xpath=//header//a[contains(text(),'Home')]";
	private static final String shop = "Shop #xpath=//li[contains(@id,'menu-item-1468')]";
	private static final String aboutUs = "Shop #xpath=//header//a[contains(text(),'About Us')]";
	private static final String learn = "Learn #xpath=//header//a[contains(text(),'Learn')]";
	private static final String contactUs = "Contact #xpath=//header//a[contains(text(),'Contact Us')]";
	private static final String privacy = "Privacy #xpath=//header//a[contains(text(),'Privacy')]";
	private static final String shop_CBDdogChews = "CBD Dog Chews #xpath=//li/a[contains(text(),'CBD Dog Chews')]";
	private static final String shop_CBDoilforCats = "CBD Oil for Cats #xpath=//li/a[contains(text(),'CBD Oil for Cats')]";
	private static final String shop_CBDoilforPets = "CBD Oil for Pets #xpath=//li/a[contains(text(),'CBD Oil for Pets')]";
	private static final String shop_CBDpawButter = "CBD Paw Butter #xpath=//li/a[contains(text(),'CBD Paw Butter')]";
	private static final String shop_CBDshampooConditioner = "CBD Shampoo & Conditioner #xpath=//li/a[contains(text(),'CBD Shampoo & Conditioner')]";
	private static final String shop_CBDbundles = "CBD Bundles #xpath=//li/a[contains(text(),'CBD Bundles')]";
//	private static final String shop_ShopAll = "Shop All #xpath=//li/a[contains(text(),'Shop All')]";
	private static final String shop_SeeProducts = "Shop All #xpath=//li/a[contains(text(),'See Products')]";

	private static final String learn_PetBlog = "Pet Blog #xpath=//li/a[contains(text(),'Pet Blog')]";
	private static final String learn_BenefitsForPets = "CBD Benefits For Pets #xpath=//li/a[contains(text(),'CBD Benefits For Pets')]";
	private static final String learn_CBDlabTestingResults = "CBD Lab Testing Results #xpath=//li/a[contains(text(),'CBD Lab Testing Results')]";
	private static final String learn_IntheNews = "In the News #xpath=//li/a[contains(text(),'In the News')]";
	private static final String learn_faq = "FAQ #xpath=//li/a[contains(text(),'FAQ')]";
	private static final String learn_PetDosingCalculator = "Pet Dosing Calculator #xpath=//li/a[contains(text(),'Pet Dosing Calculator')]";

	private static final String facebookHeaderLink = "Facebook Header Link #xpath=//li[@class='social']//a[contains(@href,'fb')]";
	private static final String instagramHeaderLink = "Instagram Header Link #xpath=//li[@class='social']//a[contains(@href,'instagram')]";
	private static final String twitterHeaderLink = "Twitter Header Link #xpath=//li[@class='social']//a[contains(@href,'twitter')]";

	private static final String contactUsHeader = "Contact Us Header #xpath=//h1[contains(text(),'Contact Us')]";
	private static final String becomeDistributorHeader = "Become a Distributor Header #xpath=//h2[contains(text(),'How to Become a Perfect Paws Hemp Wholesale')]";
	private static final String MyCartHeader = "My Cart Header #xpath=//h1[contains(text(),'Cart')]";
	private static final String MyAccountHeader = "My Cart Header #xpath=//h1[contains(text(),'My account')]";
	private static final String checkoutHeader = "My Cart Header #xpath=//h1[contains(text(),'Checkout')]";
	private static final String privacyPolicyHeader = "Privacy Policy Header #xpath=//h1[contains(text(),'Privacy Policy')]";
	private static final String emptyCartMessage = "Empty Cart Message #xpath=//p[contains(text(),'Your cart is currently empty.')]";
	private static final String slide_HomePage = "Home Banner Slide #xpath=//div[@id='panel-298-0-0-0']//img";
	private static final String shopHeader = "Shop Header #xpath=//h1[contains(text(),'Perfect Paws Hemp Premium CBD Products for Pets')]";
	private static final String CBDdogChewsHeader = "CBD Dog Chews Header #xpath=//h1[contains(text(),'CBD Dog Chews')]";
	private static final String CBDoilForCatsHeader = "CBD Oil for Cats Header #xpath=//h1[contains(text(),'CBD Oil for Cats')]";
	private static final String cbdOilForPetsHeader = "CBD Oil For Pets Header #xpath=//h1[contains(text(),'Perfect Paws CBD Pet Oil | Premium CBD Oil For Dog')]";
	private static final String CBDPawButterHeader = "CBD Paw Butter Header #xpath=//h1[contains(text(),'CBD Paw Butter')]";
	private static final String CBDshampooConditionerHeader = "CBD Shampoo & Conditioner For Pets Header #xpath=//h1[contains(text(),'CBD Shampoo & Conditioner For Pets')]";
	private static final String CBDbundleHeader = "Perfect Paws Hemp CBD Bundle Header #xpath=//h1[contains(text(),'Perfect Paws Hemp CBD Bundle')]";

	private static final String AboutUsHeader = "About Us Header #xpath=//h1[contains(text(),'About Us')]";

	private static final String petBlogHeader = "Pet Blog Header #xpath=//h1[contains(text(),'EVERYTHING PERFECT PAWS CBD BLOG')]";
	private static final String cbdBenefitsPetsHeader = "CBD Benefits For Pets Header #xpath=//h1[contains(text(),'CBD Benefits For Pets')]";
	private static final String labtestingResultsHeader = "Lab Testing Results Header #xpath=//h1[contains(text(),'CBD Lab Testing Results')]";
	private static final String intheNewsHeader = "In the News Header #xpath=//h1[contains(text(),'In the News')]";
	private static final String faqHeader = "FAQ Header #xpath=//h1[contains(text(),'FAQ | Frequently Asked Questions')]";
	private static final String petDosingHeader = "Pet Dosing Calculator Header #xpath=//h1[contains(text(),'CBD Dosing Chart For Dogs')]";

	// Footer
	private static final String companyFooterLink = "Company Footer Link #xpath=//a[contains(text(),'The Company')]";
	private static final String labResultsFooterLink = "Lab Results Footer Link #xpath=//a[contains(text(),'Lab Results')]";
	private static final String wholesaleFooterLink = "Wholesale Footer Link #xpath=//a[contains(text(),'Wholesale')]";
	private static final String myAccountFooterLink = "My Account Footer Link #xpath=//a[contains(text(),'My Account')]";
	private static final String cbdProductsFooterLink = "CBD Products Footer Link #xpath=//a[contains(text(),'CBD Products')]";
	private static final String myCartFooterLink = "My Cart Footer Link #xpath=//a[contains(text(),'My Cart')]";
	private static final String checkoutFooterLink = "Checkout Footer Link #xpath=//ul[@class='footer-menu']//a[contains(text(),'Checkout')]";
	private static final String contactFooterLink = "Contact Footer Link #xpath=//ul[@class='footer-menu']//a[contains(text(),'Contact')]";
	private static final String ccpaFooterLink = "CCPA Footer Link #xpath=//a[contains(text(),'CCPA')]";
	private static final String shippingPolicyFooterLink = "Shipping Policy Footer Link #xpath=//a[contains(text(),'Shipping Policy')]";
	private static final String refundPolicyFooterLink = "Refund Policy Footer Link #xpath=//a[contains(text(),'Refund Policy')]";
	private static final String newsletterFooterLink = "Newsletter Footer Link #xpath=//a[contains(text(),'Newsletter')]";
	private static final String seniorsProgramFooterLink = "Seniors Program Footer Link #xpath=//a[contains(text(),'Seniors Program')]";
	private static final String veteransProgramFooterLink = "Veterans Program Footer Link #xpath=//a[contains(text(),'Veterans Program')]";
	private static final String firstRespondersProgramFooterLink = "First Responders Program Footer Link #xpath=//a[contains(text(),'First Responders Program')]";

	private static final String facebookFooterLink = "Facebook Footer Link #xpath=//div[contains(@class,'footer')]//a[contains(@href,'fb')]";
	private static final String instagramFooterLink = "Instagram Footer Link #xpath=//div[contains(@class,'footer')]//a[contains(@href,'instagram')]";
	private static final String twitterFooterLink = "Twitter Footer Link #xpath=//ul[contains(@class,'social ')]//a[contains(@href,'twitter')]";

	private static final String ccpaHeader = "CCPA Header Header #xpath=//h1[contains(text(),'CCPA Data Request')]";
	private static final String shippingPolicyHeader = "Shipping Policy Header #xpath=//h1[contains(text(),'Shipping Policy')]";
	private static final String refundPolicyHeader = "Relief Policy Header #xpath=//h1[contains(text(),'REFUND POLICY')]";
	private static final String newsletterHeader = "Newsletter Header #xpath=//h1[contains(text(),'Subscribe To Our Newsletter')]";
	private static final String seniorsProgramHeader = "Seniors Program Header #xpath=//h1[contains(text(),'Sign Up For Our Senior Program')]";
	private static final String veteransHeader = "Veterans Header #xpath=//h1[contains(text(),'Join Our Veterans Program')]";
	private static final String firstRespondersHeader = "First Responders Header #xpath=//h1[contains(text(),'Join Our First Responders Program')]";

	public HeaderAndFooters(BaseClass obj) {
		super(obj);
	}

	/**
	 * Description: Method to go to Customer Service
	 */
	public void goTo_CustomerService() {
		try {
			if (isElementPresent(customerCare)) {
				highLighterMethod(customerCare);
				clickOn(customerCare);
			} else {
				testStepFailed("Link for Customer Service not present in Header");
			}
		} catch (Exception e) {
			testStepFailed("Could not go to the relevant page after clicking link for Customer Care");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to go to Become Distributor
	 */
	public void goTo_BecomeDistributor() {
		try {
			if (isElementPresent(becomeDistributor)) {
				highLighterMethod(becomeDistributor);
				clickOn(becomeDistributor);
			} else {
				testStepFailed("Become Distributor not present in Header");
			}
		} catch (Exception e) {
			testStepFailed("Could not go to Become Distributor page");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to go to my account page after User is logged in.
	 */
	public void goToMyAccount() {
		try {
			if (isElementPresent(myAccount)) {
				highLighterMethod(myAccount);
				clickOn(myAccount);
			} else {
				testStepFailed("Could not go to My Account page", "Element not present");
			}
		} catch (Exception e) {
			testStepFailed("Could not click on My Account successfully");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to verify presence of Phone Number
	 */
	public void Verifypresence_PhoneNumber() {
		try {
			if (isElementPresent(phoneNumber) && findWebElement(phoneNumber).isEnabled()) {
				highLighterMethod(phoneNumber);
				testStepInfo("The Number present in Call Us is : " + getText(phoneNumber));
			} else {
				testStepFailed("Phone Number is not present or is not enabled in Header");
			}
		} catch (Exception e) {
			testStepFailed("Presence of Phone Number in Header could not be verified");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to click on the Facebook link in Header
	 */
	public void clickFacebookIcon_Header() {
		try {
			if (isElementPresent(facebookHeaderLink)) {
				highLighterMethod(facebookHeaderLink);
				clickOn(facebookHeaderLink);
			} else {
				testStepFailed("Facebook Icon not displayed in Header");
			}
		} catch (Exception e) {
			testStepFailed("Facebook Icon in Header could not clicked");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to click on the Instagram link in Header
	 */
	public void clickInstagramIcon_Header() {
		try {
			if (isElementPresent(instagramHeaderLink)) {
				highLighterMethod(instagramHeaderLink);
				clickOn(instagramHeaderLink);
			} else {
				testStepFailed("Instagram Icon not displayed in Header");
			}
		} catch (Exception e) {
			testStepFailed("Instagram Icon in Header could not clicked");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to click on the Twitter link in Header
	 */
	public void clickTwitterIcon_Header() {
		try {
			if (isElementPresent(twitterHeaderLink)) {
				highLighterMethod(twitterHeaderLink);
				clickOn(twitterHeaderLink);
			} else {
				testStepFailed("Twitter Icon not displayed in Header");
			}
		} catch (Exception e) {
			testStepFailed("Twitter Icon in Header could not clicked");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method is used to click on My cart in Header
	 */
	public void clickOnMyCart() {

		try {
			if (isElementPresent(cart)) {
				highLighterMethod(cart);
				clickOn(cart);
			} else {
				testStepFailed("My Cart not present in Header");
			}
		} catch (Exception e) {
			testStepFailed("Cart could not be clicked successfully");
			e.printStackTrace();
		}

	}

	/**
	 * Description: Method is used to click on Checkout in Header
	 */
	public void clickOnCheckout() {

		try {
			if (isElementPresent(checkout)) {
				highLighterMethod(checkout);
				clickOn(checkout);
			} else {
				testStepFailed("Checkout not present in Header");
			}
		} catch (Exception e) {
			testStepFailed("Checkout could not be clicked successfully");
			e.printStackTrace();
		}

	}

	/**
	 * Description: Method is used to click on home in Header.
	 */
	public void goTo_Home() {

		try {
			if (isElementPresent(home)) {
				highLighterMethod(home);
				clickOn(home);
			} else {
				testStepFailed("Home not present in Header Menu");
			}
		} catch (Exception e) {
			testStepFailed("Home could not be clicked successfully");
			e.printStackTrace();
		}

	}

	/**
	 * Description: Method is used to click on desired option from Shop menu
	 */
	public void goTo_Shop(int subMenu) {

		try {
			if (subMenu == 0) {
				if (isElementPresent(shop)) {
					highLighterMethod(shop);
					clickOn(shop);
				} else {
					testStepFailed("Shop not present in Header Menu");
				}
			} else {
				switch (subMenu) {
				case 1: {
					if (isElementPresent(shop_CBDdogChews)) {
						navigateMenu_Two(shop, shop_CBDdogChews);
					} else {
						testStepFailed("CBD Dog Chews not present under Shop");
					}

					break;
				}

				case 2: {
					if (isElementPresent(shop_CBDoilforCats)) {
						navigateMenu_Two(shop, shop_CBDoilforCats);
					} else {
						testStepFailed("CBD Oil For Cats not present under Shop Menu");
					}
					break;
				}

				case 3: {
					if (isElementPresent(shop_CBDoilforPets)) {
						navigateMenu_Two(shop, shop_CBDoilforPets);
					} else {
						testStepFailed("CBD Oil For Pets not present under Shop Menu");
					}
					break;
				}

				case 4: {
					if (isElementPresent(shop_CBDpawButter)) {
						navigateMenu_Two(shop, shop_CBDpawButter);
					} else {
						testStepFailed("CBD Paw Butter not present under Shop Menu");
					}
					break;
				}

				case 5: {
					if (isElementPresent(shop_CBDshampooConditioner)) {
						navigateMenu_Two(shop, shop_CBDshampooConditioner);
					} else {
						testStepFailed("CBD Shampoo And Conditioner not present under Shop Menu");
					}
					break;
				}

				case 6: {
					if (isElementPresent(shop_CBDbundles)) {
						navigateMenu_Two(shop, shop_CBDbundles);
					} else {
						testStepFailed("CBD Bundles not present under Shop Menu");
					}
					break;
				}

				case 7: {
//					if (isElementPresent(shop_ShopAll)) {
//						navigateMenu_Two(shop, shop_ShopAll);
//					} else {
//						testStepFailed("Shop All not present under Shop Menu");
//					}
//					break;
					if (isElementPresent(shop_SeeProducts)) {
						navigateMenu_Two(shop, shop_SeeProducts);
					} else {
						testStepFailed("See Products not present under Shop Menu");
					}
					break;
				}
				}
			}
		} catch (Exception e) {
			testStepFailed("Desired option from the Shop menu could not be clicked successfully");
			e.printStackTrace();
		}

	}

	/**
	 * Description: Method is used to click on About Us in Header.
	 */
	public void goTo_AboutUs() {

		try {
			if (isElementPresent(aboutUs)) {
				highLighterMethod(aboutUs);
				clickOn(aboutUs);
			} else {
				testStepFailed("About Us not present in Header Menu");
			}
		} catch (Exception e) {
			testStepFailed("About Us could not be clicked successfully");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method is used to click on desired option from Learn menu
	 */
	public void goTo_Learn(int subMenu) {
		try {
			if (subMenu == 0) {
				if (isElementPresent(learn)) {
					highLighterMethod(learn);
					clickOn(learn);
				} else {
					testStepFailed("Learn not present in Header Menu");
				}
			} else {
				switch (subMenu) {
				case 1: {
					if (isElementPresent(learn_PetBlog)) {
						navigateMenu_Two(learn, learn_PetBlog);
					} else {
						testStepFailed("Pet Blog not present under Learn Menu");
					}
					break;
				}

				case 2: {
					if (isElementPresent(learn_BenefitsForPets)) {
						navigateMenu_Two(learn, learn_BenefitsForPets);
					} else {
						testStepFailed("Benefits For Pets not present under Learn Menu");
					}
					break;
				}

				case 3: {
					if (isElementPresent(learn_CBDlabTestingResults)) {
						scrollUp();
						navigateMenu_Two(learn, learn_CBDlabTestingResults);
					} else {
						testStepFailed("CBD Lab Testing Results not present under Learn Menu");
					}
					break;
				}
				case 4: {
					if (isElementPresent(learn_IntheNews)) {
						navigateMenu_Two(learn, learn_IntheNews);
					} else {
						testStepFailed("In the News not present under Learn Menu");
					}
					break;
				}

				case 5: {
					if (isElementPresent(learn_faq)) {
						navigateMenu_Two(learn, learn_faq);
					} else {
						testStepFailed("FAQ not present under Learn Menu");
					}
					break;
				}
				case 6: {
					if (isElementPresent(learn_PetDosingCalculator)) {
						navigateMenu_Two(learn, learn_PetDosingCalculator);
					} else {
						testStepFailed("Pet Dosing Calculator not present under Learn Menu");
					}
					break;
				}
				}
			}
		} catch (Exception e) {
			testStepFailed("Desired option from the Learn menu could not be clicked successfully");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method is used to click on contact
	 */
	public void goTo_ContactUs() {

		try {
			if (isElementPresent(contactUs)) {
				highLighterMethod(contactUs);
				clickOn(contactUs);
			} else {
				testStepFailed("Contact not present in Header Menu");
			}
		} catch (Exception e) {
			testStepFailed("Contact could not be clicked successfully");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to verify the function of privacy policy
	 */
	public void Verify_PrivacyPolicy() {
		try {
			if (isElementPresent(privacy)) {
				highLighterMethod(privacy);
				clickOn(privacy);
				if (isElementDisplayed(privacyPolicyHeader)) {
					highLighterMethod(privacyPolicyHeader);
					testStepInfo("The privacy policy page opened successfully");
				} else
					testStepFailed("The privacy policy page could not be opened");
			} else {
				testStepFailed("Privacy not present in Header");
			}
		} catch (Exception e) {
			testStepFailed("Privacy could not be verified");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method is used to verify the routing.
	 */
	public void verifyNavigation(String option) {

		try {
			switch (option) {
			case "cart": {
				if (isElementDisplayed(MyCartHeader)) {
					highLighterMethod(MyCartHeader);
					testStepInfo("The relevant page was opened upon clicking cart");
				} else {
					testStepFailed("My Cart Title was not present");
				}
				break;
			}

			case "checkout": {
				if (isElementDisplayed(checkoutHeader)) {
					highLighterMethod(checkoutHeader);
					testStepInfo("The relevant page was opened upon clicking Checkout");
				} else {
					testStepFailed("Checkout Title was not present");
				}
				break;
			}

			case "aboutUs": {
				if (isElementDisplayed(AboutUsHeader)) {
					highLighterMethod(AboutUsHeader);
					testStepInfo("The relevant page was opened upon clicking About Us");
				} else {
					testStepFailed("About Us was not present in Header");
				}
				break;
			}

			case "myAccount": {
				if (isElementDisplayed(MyAccountHeader)) {
					highLighterMethod(MyAccountHeader);
					testStepInfo("The relevant page was opened upon clicking My Account");
				} else {
					testStepFailed("My Account Title was not present");
				}
				break;
			}

			case "emptyCart": {
				if (isElementDisplayed(emptyCartMessage)) {
					highLighterMethod(emptyCartMessage);
					testStepInfo("Empty Cart message was displayed");
				} else {
					testStepFailed("Empty Cart message was not displayed");
				}
				break;
			}

			case "becomeDistributor": {
				if (isElementDisplayed(becomeDistributorHeader)) {
					highLighterMethod(becomeDistributorHeader);
					testStepInfo("The relevant page was opened upon clicking Become Distributor in Header");
				} else {
					testStepFailed("The relevant page was not opened upon clicking Become Distributor in Header");
				}
				break;
			}

			case "homeBanner": {
				waitForElementToDisplay(slide_HomePage, 60);
				if (isElementDisplayed(slide_HomePage)) {
					scrollToViewElement(slide_HomePage);
					highLighterMethod(slide_HomePage);
					testStepInfo("The relevant page was opened upon clicking Home in Header");
				} else {
					testStepFailed("The relevant page was not opened upon clicking Home in Header");
				}
				break;
			}

			case "shop": {
				if (isElementDisplayed(shopHeader)) {
					highLighterMethod(shopHeader);
					testStepInfo("The relevant page was opened upon clicking Shop in Header");
				} else {
					testStepFailed("The relevant page was not opened upon clicking Shop in Header");
				}
				break;
			}

			case "CBDdogChews": {
				if (isElementDisplayed(CBDdogChewsHeader)) {
					highLighterMethod(CBDdogChewsHeader);
					testStepInfo("The relevant page was opened upon clicking CBD Dog Chews under Shop");
				} else {
					testStepFailed("The relevant page was not opened upon clicking CBD Dog Chews under Shop");
				}
				break;
			}

			case "CBDoilForCats": {
				if (isElementDisplayed(CBDoilForCatsHeader)) {
					highLighterMethod(CBDoilForCatsHeader);
					testStepInfo("The relevant page was opened upon clicking CBD Oil For Cats under Shop");
				} else {
					testStepFailed("The relevant page was not opened upon clicking CBD Oil For Cats under Shop");
				}
				break;
			}
			case "cbdOilForPets": {
				if (isElementDisplayed(cbdOilForPetsHeader)) {
					highLighterMethod(cbdOilForPetsHeader);
					testStepInfo(
							"The relevant page was opened upon clicking CBD Gummies under CBD Oil For Pets in Shop Menu");
				} else {
					testStepFailed(
							"The relevant page was not opened upon clicking CBD Gummies under CBD Oil For Pets in Shop Menu");
				}
				break;
			}

			case "CBDPawButter": {
				if (isElementDisplayed(CBDPawButterHeader)) {
					highLighterMethod(CBDPawButterHeader);
					testStepInfo(
							"The relevant page was opened upon clicking CBD Capsules under CBD Paw Butter in Shop Menu");
				} else {
					testStepFailed(
							"The relevant page was not opened upon clicking CBD Capsules under CBD Paw Butter in Shop Menu");
				}
				break;
			}

			case "CBDshampooConditioner": {
				if (isElementDisplayed(CBDshampooConditionerHeader)) {
					highLighterMethod(CBDshampooConditionerHeader);
					testStepInfo(
							"The relevant page was opened upon clicking CBD Relaxation Shot under CBD Shampoo & Conditioner in Shop Menu");
				} else {
					testStepFailed(
							"The relevant page was not opened upon clicking CBD Relaxation Shot under CBD Shampoo & Conditioner in Shop Menu");
				}
				break;
			}

			case "CBDbundle": {
				if (isElementDisplayed(CBDbundleHeader)) {
					highLighterMethod(CBDbundleHeader);
					testStepInfo("The relevant page was opened upon clicking CBD Bundle in Shop Menu");
				} else {
					testStepFailed("The relevant page was not opened upon clicking CBD Bundle in Shop Menu");
				}
				break;
			}

			case "petBlog": {
				if (isElementDisplayed(petBlogHeader)) {
					highLighterMethod(petBlogHeader);
					testStepInfo("The relevant page was opened upon clicking Pet Blog in Learn Menu");
				} else {
					testStepFailed("The relevant page was not opened upon clicking Pet Blog in Learn Menu");
				}
				break;
			}

			case "cbdBenefitsPets": {
				if (isElementDisplayed(cbdBenefitsPetsHeader)) {
					highLighterMethod(cbdBenefitsPetsHeader);
					testStepInfo("The relevant page was opened upon clicking CBD Benefits For Pets in Learn Menu");
				} else {
					testStepFailed(
							"The relevant page was not opened upon clicking CBD Benefits For Pets in Learn Menu");
				}
				break;
			}

			case "labTesting": {
				if (isElementDisplayed(labtestingResultsHeader)) {
					highLighterMethod(labtestingResultsHeader);
					testStepInfo("The relevant page was opened upon clicking CBD Lab Testing Results in Learn Menu");
				} else {
					testStepFailed(
							"The relevant page was not opened upon clicking CBD Lab Testing Results in Learn Menu");
				}
				break;
			}

			case "intheNews": {
				if (isElementDisplayed(intheNewsHeader)) {
					highLighterMethod(intheNewsHeader);
					testStepInfo("The relevant page was opened upon clicking In The News in Learn Menu");
				} else {
					testStepFailed("The relevant page was not opened upon clicking In The News in Learn Menu");
				}
				break;
			}

			case "faq": {
				if (isElementDisplayed(faqHeader)) {
					highLighterMethod(faqHeader);
					testStepInfo("The relevant page was opened upon clicking FAQ in Learn Menu");
				} else {
					testStepFailed("The relevant page was not opened upon clicking FAQ in Learn Menu");
				}
				break;
			}

			case "petDosing": {
				if (isElementDisplayed(petDosingHeader)) {
					highLighterMethod(petDosingHeader);
					testStepInfo("The relevant page was opened upon clicking Pet Dosing Calculator in Learn Menu");
				} else {
					testStepFailed(
							"The relevant page was not opened upon clicking Pet Dosing Calculator in Learn Menu");
				}
				break;
			}

			case "contactUs": {
				if (isElementDisplayed(contactUsHeader)) {
					highLighterMethod(contactUsHeader);
					testStepInfo("The relevant page was opened upon clicking contact");
				} else {
					testStepFailed("The relevant page was not opened upon clicking contact");
				}
				break;
			}

			case "shippingPolicy": {
				if (isElementDisplayed(shippingPolicyHeader)) {
					highLighterMethod(shippingPolicyHeader);
					testStepInfo("The relevant page was opened upon clicking Shipping Policy in Footer");
				} else {
					testStepFailed("The relevant page was not opened upon clicking Shipping Policy in Footer");
				}
				break;
			}

			case "refundPolicy": {
				if (isElementDisplayed(refundPolicyHeader)) {
					highLighterMethod(refundPolicyHeader);
					testStepInfo("The relevant page was opened upon clicking Refund Policy in Footer");
				} else {
					testStepFailed("The relevant page was not opened upon clicking Refund Policy in Footer");
				}
				break;
			}

			case "newsletter": {
				if (isElementDisplayed(newsletterHeader)) {
					highLighterMethod(newsletterHeader);
					testStepInfo("The relevant page was opened upon clicking Newsletter in Footer");
				} else {
					testStepFailed("The relevant page was not opened upon clicking Newsletter in Footer");
				}
				break;
			}

			case "seniorsProgram": {
				if (isElementDisplayed(seniorsProgramHeader)) {
					highLighterMethod(seniorsProgramHeader);
					testStepInfo("The relevant page was opened upon clicking Seniors Program in Footer");
				} else {
					testStepFailed("The relevant page was not opened upon clicking Seniors Program in Footer");
				}
				break;
			}

			case "veteransProgram": {
				if (isElementDisplayed(veteransHeader)) {
					highLighterMethod(veteransHeader);
					testStepInfo("The relevant page was opened upon clicking Veterans Program in Footer");
				} else {
					testStepFailed("The relevant page was not opened upon clicking Veterans Program in Footer");
				}
				break;
			}

			case "firstResponders": {
				if (isElementDisplayed(firstRespondersHeader)) {
					highLighterMethod(firstRespondersHeader);
					testStepInfo("The relevant page was opened upon clicking First Responders in Footer");
				} else {
					testStepFailed("The relevant page was not opened upon clicking First Responders in Footer");
				}
				break;
			}

			case "facebook": {
				switchToLastTab();
				if (driver.getCurrentUrl().contains("https://www.facebook.com/PerfectPawsHemp")) {
					testStepInfo("Facebook page of HempBombs was opened");
					driver.close();
					switchToLastTab();
				} else {
					switchToLastTab();
					testStepFailed("Facebook page of HempBombs could not be opened");
				}
				break;
			}

			case "instagram": {
				switchToLastTab();
				if (driver.getCurrentUrl().contains("https://www.instagram.com/perfectpawshemp")
						|| driver.getCurrentUrl().contains("https://www.instagram.com/accounts/login")) {
					testStepInfo("Instagram page of HempBombs was opened");
					driver.close();
					switchToLastTab();
				} else {
					switchToLastTab();
					testStepFailed("Instagram page of HempBombs could not be opened");
				}
				break;
			}

			case "twitter": {
				switchToLastTab();
				if (driver.getCurrentUrl().contains("https://twitter.com/perfectpawshemp")) {
					testStepInfo("Twitter page of HempBombs was opened");
					driver.close();
					switchToLastTab();
				} else {
					switchToLastTab();
					testStepFailed("Twitter page of HempBombs could not be opened");
				}
				break;
			}

			case "facebookFooter": {
				if (driver.getCurrentUrl().contains("https://www.facebook.com/PerfectPawsHemp")) {
					testStepInfo("Facebook page of HempBombs was opened");
					driver.navigate().back();
				} else {
					driver.navigate().back();
					testStepFailed("Facebook page of HempBombs could not be opened");
				}
				break;
			}

			case "instagramFooter": {
				if (driver.getCurrentUrl().contains("https://www.instagram.com/perfectpawshemp")
						|| driver.getCurrentUrl().contains("https://www.instagram.com/accounts/login")) {
					testStepInfo("Instagram page of HempBombs was opened");
					driver.navigate().back();
				} else {
					driver.navigate().back();
					testStepFailed("Instagram page of HempBombs could not be opened");
				}
				break;
			}

			case "twitterFooter": {
				if (driver.getCurrentUrl().contains("https://twitter.com/perfectpawshemp")) {
					testStepInfo("Twitter page of HempBombs was opened");
					driver.navigate().back();
				} else {
					driver.navigate().back();
					testStepFailed("Twitter page of HempBombs could not be opened");
				}
				break;
			}

			case "ccpa": {
				switchToLastTab();
				if (isElementDisplayed(ccpaHeader)) {
					highLighterMethod(ccpaHeader);
					testStepInfo("The relevant page was opened upon clicking CCPA in Footer");
					driver.close();
					switchToLastTab();
				} else {
					switchToLastTab();
					testStepFailed("The relevant page was not opened upon clicking CCPA in Footer");
				}
				break;
			}

			}
		} catch (Exception e) {
			testStepFailed("Relevant verification could not be done successfully");
			e.printStackTrace();
		}

	}

	/**
	 * Description: Method is used to navigate through Footer.
	 */
	public void navigateFooter(String option) {

		try {
			switch (option) {
			case "company": {
				if (isElementDisplayed(companyFooterLink)) {
					highLighterMethod(companyFooterLink);
					clickOn(companyFooterLink);
				} else {
					testStepFailed("Company not displayed in Footer");
				}
				break;
			}

			case "labResults": {
				if (isElementPresent(labResultsFooterLink)) {
					highLighterMethod(labResultsFooterLink);
					clickOn(labResultsFooterLink);
				} else {
					testStepFailed("Lab Results not displayed in Footer");
				}
				break;
			}

			case "wholesale": {
				if (isElementPresent(wholesaleFooterLink)) {
					highLighterMethod(wholesaleFooterLink);
					clickOn(wholesaleFooterLink);
				} else {
					testStepFailed("Wholesale was not displayed in Footer");
				}
				break;
			}

			case "myAccount": {
				if (isElementPresent(myAccountFooterLink)) {
					highLighterMethod(myAccountFooterLink);
					clickOn(myAccountFooterLink);
				} else {
					testStepFailed("My Account not displayed in Footer");
				}
				break;
			}

			case "cbdProducts": {
				if (isElementPresent(cbdProductsFooterLink)) {
					highLighterMethod(cbdProductsFooterLink);
					clickOn(cbdProductsFooterLink);
				} else {
					testStepFailed("CBD Products not displayed in Footer");
				}
				break;
			}

			case "myCart": {
				if (isElementPresent(myCartFooterLink)) {
					highLighterMethod(myCartFooterLink);
					clickOn(myCartFooterLink);
				} else {
					testStepFailed("My Cart not displayed in Footer");
				}
				break;
			}

			case "checkout": {
				if (isElementPresent(checkoutFooterLink)) {
					highLighterMethod(checkoutFooterLink);
					clickOn(checkoutFooterLink);
				} else {
					testStepFailed("CBD Products not displayed in Footer");
				}
				break;
			}

			case "contact": {
				if (isElementPresent(contactFooterLink)) {
					highLighterMethod(contactFooterLink);
					clickOn(contactFooterLink);
				} else {
					testStepFailed("Contact not displayed in Footer");
				}
				break;
			}

			case "ccpa": {
				if (isElementPresent(ccpaFooterLink)) {
					highLighterMethod(ccpaFooterLink);
					clickOn(ccpaFooterLink);
				} else {
					testStepFailed("CCPA not displayed in Footer");
				}
				break;
			}

			case "shippingPolicy": {
				if (isElementPresent(shippingPolicyFooterLink)) {
					highLighterMethod(shippingPolicyFooterLink);
					clickOn(shippingPolicyFooterLink);
				} else {
					testStepFailed("Shipping Policy not displayed in Footer");
				}
				break;
			}

			case "refundPolicy": {
				if (isElementPresent(refundPolicyFooterLink)) {
					highLighterMethod(refundPolicyFooterLink);
					clickOn(refundPolicyFooterLink);
				} else {
					testStepFailed("Refund Policy not displayed in Footer");
				}
				break;
			}

			case "newsletter": {
				if (isElementPresent(newsletterFooterLink)) {
					highLighterMethod(newsletterFooterLink);
					clickOn(newsletterFooterLink);
				} else {
					testStepFailed("Newsletter not displayed in Footer");
				}
				break;
			}

			case "seniorsProgram": {
				if (isElementPresent(seniorsProgramFooterLink)) {
					highLighterMethod(seniorsProgramFooterLink);
					clickOn(seniorsProgramFooterLink);
				} else {
					testStepFailed("Seniors Program not displayed in Footer");
				}
				break;
			}

			case "veteransProgram": {
				if (isElementPresent(veteransProgramFooterLink)) {
					highLighterMethod(veteransProgramFooterLink);
					clickOn(veteransProgramFooterLink);
				} else {
					testStepFailed("Veterans Program not displayed in Footer");
				}
				break;
			}

			case "firstRespondersProgram": {
				if (isElementPresent(firstRespondersProgramFooterLink)) {
					highLighterMethod(firstRespondersProgramFooterLink);
					clickOn(firstRespondersProgramFooterLink);
				} else {
					testStepFailed("First Responders Program not displayed in Footer");
				}
				break;
			}

			case "facebook": {
				if (isElementPresent(facebookFooterLink)) {
					highLighterMethod(facebookFooterLink);
					clickOn(facebookFooterLink);
				} else {
					testStepFailed("Facebook Icon not displayed in Footer");
				}
				break;
			}

			case "instagram": {
				if (isElementPresent(instagramFooterLink)) {
					highLighterMethod(instagramFooterLink);
					clickOn(instagramFooterLink);
				} else {
					testStepFailed("Instagram Icon not displayed in Footer");
				}
				break;
			}

			case "twitter": {
				if (isElementPresent(twitterFooterLink)) {
					highLighterMethod(twitterFooterLink);
					clickOn(twitterFooterLink);
				} else {
					testStepFailed("Twitter Icon not displayed in Footer");
				}
				break;
			}
			}
		} catch (Exception e) {
			testStepFailed("Requested navigation in footer could not be done");
			e.printStackTrace();
		}
	}
}
