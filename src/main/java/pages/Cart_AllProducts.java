package pages;

import java.util.ArrayList;

import org.openqa.selenium.WebElement;

import baseClass.BaseClass;
import iSAFE.ApplicationKeywords;
import iSAFE.GOR;

public class Cart_AllProducts extends ApplicationKeywords {

	private static final String getAllProducts = "All Products #xpath=//ul[contains(@class,'products')]/li/a[1]";
	private static final String getAllProducts_addToCartInPGP = "Products with Add To Cart in PGP #xpath=//a[contains(text(),'Add to cart')]";
	private static final String addToCart = "Add To Cart #xpath=//button[contains(text(),'Add to cart')]";
	private static final String selectFlavor = "Select Gummy Count #xpath=//select[@id='pa_flavor']";
	private static final String selectStrength = "Select Gummy Count #xpath=//select[@id='pa_strength']";
	private static final String viewCart = "View Cart #xpath=//a[contains(text(),'View cart')]";
	private static final String title_selectedProduct = "Title of product selected #xpath=//h1[contains(@class,'title')]";
	private static final String productAddedCart_PGP = "Title of product in cart from PGP #xpath=//a[contains(text(),'View cart')]/../a/h2";

	public Cart_AllProducts(BaseClass obj) {
		super(obj);
	}

	/**
	 * Description: Method to select the first product on the page
	 */
	public void selectFirstProduct() {
		try {
			if (isElementDisplayed(getAllProducts)) {
				java.util.List<WebElement> allProducts = new ArrayList<WebElement>();
				allProducts = findWebElements(getAllProducts);
				scrollToViewElement(allProducts.get(0));
				waitTime(3);
				allProducts.get(0).click();
				testStepInfo("The first product " + "under Gummies is clicked");
			} else {
				testStepFailed("Products were not displayed under Gummies");
			}
		} catch (Exception e) {
			testStepFailed("First Product could not be selected");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to add the first product on the PGP with AddToCart
	 */
	public String selectFirstProductWithAddToCart() {
		try {
			if (isElementDisplayed(getAllProducts_addToCartInPGP)) {
				java.util.List<WebElement> allProducts = new ArrayList<WebElement>();
				allProducts = findWebElements(getAllProducts_addToCartInPGP);
				scrollToViewElement(allProducts.get(0));
				waitTime(3);
				allProducts.get(0).click();
				testStepInfo("The first product with Add To Cart is clicked");
				waitForElementToDisplay(viewCart, 5);
				if (isElementDisplayed(viewCart)) {
					testStepInfo("The first product with Add To Cart in PGP is added to cart successfully");
					scrollToViewElement(productAddedCart_PGP);
					String title = getText(productAddedCart_PGP);
					return title;
				}

			} else {
				testStepFailed("Products with Add To Cart were not displayed in PGP");
			}
		} catch (Exception e) {
			testStepFailed("A Product with Add To Cart in PGP could not be selected");
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Description: Method to select the second product on the page
	 */
	public void selectSecondProduct() {
		try {
			if (isElementDisplayed(getAllProducts)) {
				java.util.List<WebElement> allProducts = new ArrayList<WebElement>();
				allProducts = findWebElements(getAllProducts);
				scrollToViewElement(allProducts.get(1));
				waitTime(3);
				allProducts.get(1).click();
				testStepInfo("The first product under Gummies is clicked");
			} else {
				testStepFailed("Products were not displayed under Gummies");
			}
		} catch (Exception e) {
			testStepFailed("First Product could not be selected");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to choose Flavor and Strength, accordingly
	 */
	public void chooseFlavorAndStrength() {
		try {
			if (findWebElements(selectFlavor).size() > 0) {
				if (isElementDisplayed(selectFlavor)) {
					highLighterMethod(selectFlavor);
					selectFromDropdown(selectFlavor, 1);
					testStepInfo("First option under select flavor was selected");
				} else {
					testStepFailed("Select flavor - was not displayed");
				}
			}

			if (findWebElements(selectStrength).size() > 0) {
				if (isElementDisplayed(selectStrength)) {
					highLighterMethod(selectStrength);
					selectFromDropdown(selectStrength, 1);
					testStepInfo("First option under select strength was selected");
				} else {
					testStepFailed("Select strength - was not displayed");
				}
			}
		} catch (Exception e) {
			testStepFailed("Could not select flavor and strength");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to click on Add to Cart
	 */
	public void clickAddToCart() {
		try {
			if (isElementDisplayed(addToCart)) {
				highLighterMethod(addToCart);
				clickOn(addToCart);
				testStepInfo("The add to cart button was clicked");
			} else {
				testStepFailed("The add to cart button was not displayed");
			}
		} catch (Exception e) {
			testStepFailed("Add to cart could not be clicked");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to click on View Cart
	 */
	public void clickViewCart() {
		try {
			if (isElementDisplayed(viewCart)) {
				highLighterMethod(viewCart);
				clickOn(viewCart);
				GOR.productAdded = true;
				testStepInfo("The View Cart button was clicked");
			} else {
				testStepFailed("The View Cart button was not displayed");
			}
		} catch (Exception e) {
			testStepFailed("View cart could not be clicked");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to get title of the product after selecting it.
	 */
	public String getProductTitle() {
		try {
			if (isElementDisplayed(title_selectedProduct)) {
				highLighterMethod(title_selectedProduct);
				return getText(title_selectedProduct);
			} else {
				testStepFailed("Product Title of the selected product not displayed");
			}
		} catch (Exception e) {
			testStepFailed("Could not get the product title");
			e.printStackTrace();
		}
		return "";
	}
}
