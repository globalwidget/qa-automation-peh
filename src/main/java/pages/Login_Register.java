package pages;

import baseClass.BaseClass;
import iSAFE.ApplicationKeywords;
import iSAFE.GOR;

public class Login_Register extends ApplicationKeywords {

	private static final String inputUsername_Login = "Login Username #id=username";
	private static final String inputPassword_Login = "Login Password #id=password";
	private static final String loginButton = "Login Button #xpath=//button[contains(text(),'Log in')]";
	private static final String error_EmptyCredntials = "Login Empty Credntials Error Message #xpath=//strong[contains(text(),'Error')]/..";
	private static final String error_InvalidCredentials = "Login Invalid Credentials Error Message #xpath=//strong[contains(text(),'ERROR')]/..";
	private static final String error_lostYourPassword = "Lost Password Error Message #xpath=//li/a[contains(text(),'Lost your password')]";
	private static final String inputUsername_Register = "Register Username #xpath=//input[@id='reg_email']";
	private static final String inputPassword_Register = "Register Password #xpath=//input[@id='reg_password']";
	private static final String registerButton = "Register Button #xpath=//button[contains(text(),'Register')]";
	private static final String passwordHint = "Hint #xpath=//small[contains(text(),'Hint: The password should be at least twelve chara')]";

	public Login_Register(BaseClass obj) {
		super(obj);
	}

	/**
	 * Description: Method is used to login
	 * 
	 */
	public void login(String loginUsername, String loginPassword) {
		try {
			waitForElementToDisplay(inputUsername_Login, 20);
			if (isElementDisplayed(loginButton)) {
				highLighterMethod(inputUsername_Login);
				typeIn(inputUsername_Login, loginUsername);
				highLighterMethod(inputPassword_Login);
				typeIn(inputPassword_Login, loginPassword);
				highLighterMethod(loginButton);
				testStepInfo("Valid Credentials entered");
				clickOn(loginButton);
				GOR.loggedIn = true;
				testStepInfo("Login button clicked successfully");
			} else {
				testStepFailed("Login Page not displayed");
			}
		} catch (Exception e) {
			testStepFailed("Could not login successfully");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method is used to register
	 * 
	 */
	public void register(String registerUsername, String registerPassword) {
		try {
			if (isElementDisplayed(loginButton)) {
				highLighterMethod(inputPassword_Register);
				typeIn(inputPassword_Register, registerPassword);
				highLighterMethod(inputUsername_Register);
				typeIn(inputUsername_Register, registerUsername);
				highLighterMethod(registerButton);
				testStepInfo("Valid Credentials entered");
				clickOn(registerButton);

			} else {
				testStepFailed("Register Page not displayed");
			}
		} catch (Exception e) {
			testStepFailed("Could not register successfully");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method is used to enter Password
	 * 
	 */
	public void typePassword_Register(String registerPassword) {
		try {
			if (isElementDisplayed(inputPassword_Register)) {
				highLighterMethod(inputPassword_Register);
				typeIn(inputPassword_Register, registerPassword);
			} else {
				testStepFailed("Password field for Register noot displayed");
			}
		} catch (Exception e) {
			testStepFailed("Password could not be entered for Register successfully");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to verify presence of Error Message for invalid login
	 */
	public void verifyErrorMessage_Login(String option) {
		try {
			switch (option) {
			case "usernameEmpty": {
				if (isElementPresent(error_EmptyCredntials)) {
					String value = getText(error_EmptyCredntials);
					if (value.contains("Username is required")) {
						highLighterMethod(error_EmptyCredntials);
						manualScreenshot("Error Message for Empty Username field is displayed");
					}
				} else {
					testStepFailed("Error Message for empty Username field not displayed");
				}
				break;
			}
			case "passwordEmpty": {
				if (isElementPresent(error_EmptyCredntials)) {
					String value = getText(error_EmptyCredntials);
					if (value.contains("The password field is empty")) {
						highLighterMethod(error_EmptyCredntials);
						manualScreenshot("Error Message for Empty password field is displayed");
					}
				} else {
					testStepFailed("Error Message for empty password field not displayed");
				}
				break;
			}

			case "invalidCredentials": {
				if (isElementPresent(error_lostYourPassword)) {
					String value = getText(error_InvalidCredentials);
					if (value.contains("username or password you entered is incorrect")) {
						highLighterMethod(error_InvalidCredentials);
						manualScreenshot("Error Message for Invalid Credentials is displayed");
					}
				} else {
					testStepFailed("Error Message for Invalid Credentials not displayed");
				}
				break;
			}

			}
		} catch (Exception e) {
			testStepFailed("Could not verify error message for Invalid Login Credentials successfully");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to verify presence of Error Messages for Register
	 */
	public void verifyErrorMessage_Register(String option) {
		try {
			switch (option) {
			case "passwordEmpty": {
				if (isElementPresent(error_EmptyCredntials)) {
					String value = getText(error_EmptyCredntials);
					if (value.contains("Please enter an account password")) {
						highLighterMethod(error_EmptyCredntials);
						manualScreenshot("Error  Message for Empty password field is displayed");
					}
				} else {
					testStepFailed("Error Message for empty password field not displayed");
				}
				break;
			}

			case "existingCredentials": {
				if (isElementPresent(error_EmptyCredntials)) {
					String value = getText(error_EmptyCredntials);
					if (value.contains("account is already registered")) {
						highLighterMethod(error_EmptyCredntials);
						manualScreenshot("Error Message for Existing Credentials is displayed");
					}
				} else {
					testStepFailed("Error Message for Existing Credentials not displayed");
				}
				break;
			}

			case "invalidCredentials": {
				if (isElementPresent(error_lostYourPassword)) {
					String value = getText(error_EmptyCredntials);
					if (value.contains("username or password you entered is incorrect")) {
						highLighterMethod(error_EmptyCredntials);
						manualScreenshot("Error Message for Invalid Credentials is displayed");
					}
				} else {
					testStepFailed("Error Message for Invalid Credentials not displayed");
				}
				break;
			}

			}
		} catch (Exception e) {
			testStepFailed("Could not verify error message for Invalid Register Credentials successfully");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to verify invalid password restriction for Register
	 */
	public void verifyInvalidPasswordRestriction_Resgister() {
		try {
			if (isElementDisplayed(registerButton)) {
				waitTime(2);
				String value = getAttributeValue(registerButton, "disabled");
				if (value.contains("true") && isElementDisplayed(passwordHint)) {
					testStepInfo("Invalid Password restriction verified");
				} else {
					testStepFailed("Invalid Password restriction could not be verified");
				}
			} else {
				testStepFailed("Register Button not present");
			}
		} catch (Exception e) {
			{
				testStepFailed("Invalid Password restriction could not be verified");
				e.printStackTrace();

			}

		}

	}

	/**
	 * Description: Method is used to check if the user is logged Out.
	 */

	public void verifyPresenceOfLogin() {

		try {
			if (isElementPresent(loginButton) == true) {
				testStepInfo("User is not logged In");
			} else {
				testStepFailed("Logout verification failed");
			}
		} catch (Exception e) {
			testStepFailed("Presence of Login button could not be verified");
			e.printStackTrace();
		}

	}
}
