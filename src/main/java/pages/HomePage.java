package pages;

import baseClass.BaseClass;
import iSAFE.ApplicationKeywords;
import iSAFE.GOR;

public class HomePage extends ApplicationKeywords {

	private static final String closeOfferPopup = "Close Offer Pop up #xpath=//button[contains(@alt,'Close')]";
	private static final String securityLogin_Password = "Password for Security Login #xpath=//input[@id='password_protected_pass']";
	private static final String securityLogin_LoginButton = "Login Button for Security Login #xpath=//input[@id='wp-submit']";
	private static final String companyFooterLink = "Company Footer Link #xpath=//a[contains(text(),'The Company')]";

	public HomePage(BaseClass obj) {
		super(obj);
	}

	/**
	 * Description: Method to handle the offer pop-up
	 */
	public void closeOfferPopup() {
		try {
			scrollToViewElement(companyFooterLink);
			waitForElementToDisplay(closeOfferPopup, 6);
			if (GOR.OfferPopUpHandled == false) {
				if (isElementPresent(closeOfferPopup)) {
					GOR.OfferPopUpHandled = true;
					clickOn(closeOfferPopup);
				} else {
					testStepInfo("Offer pop up not present");
				}
			} else
				testStepInfo("Offer Popup already handled");
		} catch (Exception e) {
			testStepInfo("Offer pop up not present");
		}
	}

	/**
	 * Description: Method to handle security Login
	 */
	public void securityLogin(String password) {
		try {
			if (isElementPresent(securityLogin_Password)) {
				typeIn(securityLogin_Password, password);
				clickOn(securityLogin_LoginButton);
				GOR.securityLogin = true;

			} else {
				testStepFailed("Security Login password could not be entered", "Element not present");
			}
		} catch (Exception e) {
			testStepFailed("Could not perform the security Login");
			e.printStackTrace();
		}
	}

//	/**
//	 * Description: Method to handle security Login
//	 */
//	public boolean checkSecurityLoginPresence() {
//		try {
//			if (findWebElements(closeOfferPopup).size() > 0)
//				return true;
//			else
//				return false;
//		} catch (Exception e) {
//			testStepFailed("Could not check pop up presence");
//			e.printStackTrace();
//		}
//		return false;
//	}
}
