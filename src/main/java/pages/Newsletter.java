package pages;

import baseClass.BaseClass;
import iSAFE.ApplicationKeywords;

public class Newsletter extends ApplicationKeywords {

	private static final String email = "Email #xpath=//input[@type='email']";
	private static final String emptyEmailAlert = "Empty Email Alert #xpath=//input[@type='email']/..//span[contains(text(),'This field is required')]";
	private static final String invalidEmailAlert = "Invalid Email Alert Newsletter #xpath=//span[contains(text(),'Please enter a valid email address')]";
	private static final String signAndSave = "Submit  #xpath=//button[contains(text(),'SIGN & SAVE')]";
	private static final String successMessage = "Success Message #xpath=//h1[contains(text(),'Thank You For Subscribing To Our Newsletter')]";

	public Newsletter(BaseClass obj) {
		super(obj);
	}

	/**
	 * Description: Method to enter details for Newsletter Subscription
	 */
	public void enterDetails(String email, String firstName, String birthday) {
		try {
			if (isElementDisplayed(Newsletter.email)) {
				highLighterMethod(Newsletter.email);
				typeIn(Newsletter.email, email);
			} else {
				testStepFailed("Could not enter email for Newsletter Subscription", "Element not present");
			}
		} catch (Exception e) {
			testStepFailed("Could not enter details for Newsletter Subscription");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to verify empty field errors
	 */
	public void verifyEmptyFieldErrors() {
		try {
			if (isElementDisplayed(emptyEmailAlert)) {
				highLighterMethod(emptyEmailAlert);
				testStepInfo("The Error for empty email field was displayed");
			} else {
				testStepFailed("The Error for empty email field was not displayed", "Element not present");
			}
		} catch (Exception e) {
			testStepFailed("The Empty field errors could not be validated");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to verify invalid email error
	 */
	public void verifyInvalidEmailErrorNewsletter() {
		try {
			if (isElementDisplayed(invalidEmailAlert)) {
				highLighterMethod(invalidEmailAlert);
				testStepInfo("The Error for invalid email was displayed");
			} else {
				testStepFailed("The Error for invalid email was not displayed", "Element not present");
			}
		} catch (Exception e) {
			testStepFailed("The Error for invalid email was not displayed");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to click on 'Get 15% off'
	 */
	public void clickSignAndSave() {
		try {
			if (isElementDisplayed(signAndSave)) {
				highLighterMethod(signAndSave);
				clickOn(signAndSave);
			} else {
				testStepFailed("Could not click on 'Get 15% off'", "Element not present");
			}
		} catch (Exception e) {
			testStepFailed("Could not click on 'Get 15% off'");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to verify Success Message for Newsletter Subscription
	 */
	public void verifySuccessMessage() {
		try {
			if (isElementDisplayed(successMessage)) {
				highLighterMethod(successMessage);
				testStepInfo("The Success Message for Newsletter Subscription was displayed");
			} else {
				testStepFailed("The Success Message for Newsletter Subscription was not displayed",
						"Element not present");
			}
		} catch (Exception e) {
			testStepFailed("The Success Message for Newsletter Subscription could not be validated");
			e.printStackTrace();
		}
	}
}
