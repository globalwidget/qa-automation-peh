package pages;

import java.util.HashMap;

import baseClass.BaseClass;
import iSAFE.ApplicationKeywords;
import iSAFE.GOR;

public class MyAccount extends ApplicationKeywords {

	private static final String logoutButton = "Logout Button #xpath=//a[contains(text(),'Log out')]";
	private static final String addresses = "Addresses #xpath=//a[contains(text(),'Addresses')]";
	private static final String edit_Billing = "Edit Billing Adress #xpath=//a[@class='edit' and contains(@href,'billing')]";
	private static final String edit_Shipping = "Edit Shipping Adress #xpath=//a[@class='edit' and contains(@href,'shipping')]";
	private static final String firstName_Billing = "First Name Billing #id=billing_first_name";
	private static final String lastName_Billing = "Last Name Billing #id=billing_last_name";
	private static final String phoneNumber_Billing = "Phone Number Billing #id=billing_phone";
	private static final String country_Billing = "Country Billing #id=select2-billing_country-container";
	private static final String streetAddress_Billing = "Street Address Billing #id=billing_address_1";
	private static final String streetAddressNextLine_Billing = "Street Address Next Line Billing #id=billing_address_2";
	private static final String postCode_Billing = "Post Code Billing #id=billing_postcode";
	private static final String city_Billing = "City Billing #id=billing_city";
	private static final String state_Billing = "State Billing #id=select2-billing_state-container";
	private static final String email_Billing = "Email Billing #id=billing_email";
	private static final String firstName_Shipping = "First Name Shipping #id=shipping_first_name";
	private static final String lastName_Shipping = "Last Name Shipping #id=shipping_last_name";
	private static final String country_Shipping = "Country Shipping #id=select2-shipping_country-container";
	private static final String streetAddress_Shipping = "Street Address Shipping #id=shipping_address_1";
	private static final String streetAddressNextLine_Shipping = "Street Address Next Line Shipping #id=shipping_address_2";
	private static final String postCode_Shipping = "Post Code Shipping #id=shipping_postcode";
	private static final String city_Shipping = "City Shipping #id=shipping_city";
	private static final String state_Shipping = "State Shipping #id=select2-shipping_state-container";

	public MyAccount(BaseClass obj) {
		super(obj);
	}

	/**
	 * Description: Method is used to logout
	 */
	public void clickOnLogout() {

		try {
			if (isElementPresent(logoutButton)) {
				highLighterMethod(logoutButton);
				clickOn(logoutButton);
				GOR.loggedIn = false;
				testStepInfo("Logged Out");
			} else {
				testStepFailed("Logout Button Not present");
			}
		} catch (Exception e) {
			testStepFailed("Logout could not be performed");
			e.printStackTrace();
		}

	}

	/**
	 * Description: Method is used to go to addresses
	 */
	public void clickOnAddresses() {

		try {
			if (isElementPresent(addresses)) {
				highLighterMethod(addresses);
				clickOn(addresses);
			} else {
				testStepFailed("Addresses is not present");
			}
		} catch (Exception e) {
			testStepFailed("Addresses could not be clicked");
			e.printStackTrace();
		}

	}

	/**
	 * Description: Method to get saved billing details
	 */
	public HashMap<String, String> getSavedBilllingDetails() {
		HashMap<String, String> billingDetails = null;
		try {
			billingDetails = new HashMap<String, String>();
			if (isElementDisplayed(edit_Billing)) {
				highLighterMethod(edit_Billing);
				clickOn(edit_Billing);
				String[] fields = { firstName_Billing, lastName_Billing, country_Billing, streetAddress_Billing,
						streetAddressNextLine_Billing, city_Billing, state_Billing, postCode_Billing,
						phoneNumber_Billing, email_Billing };
				if (isElementPresent(firstName_Billing)) {
					for (int i = 0; i < fields.length; i++) {
						if (isElementDisplayed(fields[i])) {
							if (fields[i].contains("Country") || fields[i].contains("State")) {
								{
									String value = getText(fields[i]);
									highLighterMethod(fields[i]);
									billingDetails.put(fields[i], value);
								}
							} else {
								String value = findWebElement(fields[i]).getAttribute("value");
								highLighterMethod(fields[i]);
								billingDetails.put(fields[i], value);
							}
						} else {
							testStepFailed(fields[i].split("#")[0] + " input field was not displayed");
						}
					}
				} else {
					testStepFailed("First Name field was not present");
				}
			} else {
				testStepPassed("The edit button for billing was not present");
			}
		} catch (Exception e) {
			testStepFailed("Saved Billing Details could not be fetched successfully");
			e.printStackTrace();
		}
		return billingDetails;

	}

	/**
	 * Description: Method to get saved shipping details
	 */
	public HashMap<String, String> getSavedShippingDetails() {
		HashMap<String, String> shippingDetails = null;
		try {
			shippingDetails = new HashMap<String, String>();
			if (isElementDisplayed(edit_Shipping)) {
				highLighterMethod(edit_Shipping);
				clickOn(edit_Shipping);
				String[] fields = { firstName_Shipping, lastName_Shipping, country_Shipping, streetAddress_Shipping,
						streetAddressNextLine_Shipping, city_Shipping, state_Shipping, postCode_Shipping };
				if (isElementPresent(firstName_Shipping)) {
					for (int i = 0; i < fields.length; i++) {
						if (isElementDisplayed(fields[i])) {
							if (fields[i].contains("Country") || fields[i].contains("State")) {
								highLighterMethod(fields[i]);
								shippingDetails.put(fields[i], getText(fields[i]));
							} else {
								highLighterMethod(fields[i]);
								shippingDetails.put(fields[i], getAttributeValue(fields[i], "value"));
							}
						} else {
							testStepFailed(fields[i].split("#")[0] + " input field was not displayed");
						}
					}
				} else {
					testStepFailed("First Name field was not present");
				}
			} else {
				testStepPassed("The edit button for shipping was not present");
			}
		} catch (Exception e) {
			testStepFailed("Saved Shipping Details could not be fetched successfully");
			e.printStackTrace();
		}
		return shippingDetails;

	}

	/**
	 * Description: Method is used verify whether the user is logged In
	 */

	public void verifyPresenceOfLogout() {

		try {
			if (isElementPresent(logoutButton)) {
				highLighterMethod(logoutButton);
				testStepInfo("User is logged In");
			} else {
				testStepFailed("User login verification successful");
			}
		} catch (Exception e) {
			testStepFailed("Logout presence could not be verified successfully");
			e.printStackTrace();
		}

	}
}
