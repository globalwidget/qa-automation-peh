package pages;

import baseClass.BaseClass;
import iSAFE.ApplicationKeywords;

public class ContactUs extends ApplicationKeywords {

	private static final String contactName = "Contact Name #id=name";
	private static final String email = "Email #id=email";
	private static final String phone = "Phone #id=phone";
	private static final String subject = "Subject #id=subject";
	private static final String description = "Description #xpath=//textarea[@name='description']";
	private static final String submit = "Submit #xpath=//input[@type='submit']";

	private static final String oneOrMoreFields_Error = "One or More Fields Error #xpath=//form/div[contains(text(),'One or more fields have an error.')]";
	private static final String department_EmptyError = "Department Empty Error#xpath=//select[@name='your-recipient']//following-sibling::span[contains(text(),'The field is required.')]";
	private static final String firstName_EmptyError = "First Name Empty Error #xpath=//input[@name='first-name']//following-sibling::span[contains(text(),'The field is required.')]";
	private static final String lastName_EmptyError = "Last Name Empty Error #xpath=//input[@name='last-name']//following-sibling::span[contains(text(),'The field is required.')]";
	private static final String email_EmptyError = "Email Empty Error #xpath=//input[@type='email']//following-sibling::span[contains(text(),'The field is required.')]";
	private static final String phone_EmptyError = "Phone Empty Error #xpath=//input[@type='tel']//following-sibling::span[contains(text(),'The field is required.')]";
	private static final String EmailAddressInvalidError = "Email Address Invalid Error #xpath=//span[contains(text(),'The e-mail address entered is invalid.')]";
	private static final String telephoneInvalidInvalidError = "Telephone Invalid Error #xpath=//span[contains(text(),'The telephone number is invalid.')]";
	private static final String successMessage = "Success Message #xpath=//h2[contains(text(),'SUCCESS')]";

	public ContactUs(BaseClass obj) {
		super(obj);
	}

	/**
	 * Description: Method is used to enter details in the form
	 * 
	 */
	public void fillDetails(String contactName_data, String email_data, String phone_data, String subject_data,
			String description_data) {
		try {
			if (isElementDisplayed(contactName)) {
				highLighterMethod(contactName);
				typeIn(contactName, contactName_data);
			}

			if (isElementDisplayed(email) && !(email_data.isEmpty())) {
				highLighterMethod(email);
				typeIn(email, email_data);
			}

			if (isElementDisplayed(phone) && !(phone_data.isEmpty())) {
				highLighterMethod(phone);
				typeIn(phone, phone_data);
			}

			if (isElementDisplayed(subject) && !(subject_data.isEmpty())) {
				highLighterMethod(subject);
				typeIn(subject, subject_data);
			}

			if (isElementDisplayed(description) && !(description_data.isEmpty())) {
				highLighterMethod(description);
				typeIn(description, description_data);
			}

		} catch (Exception e) {
			testStepFailed("Could not fill details successfully");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method is used to click on Send
	 */
	public void clickSend() {
		try {
			if (isElementDisplayed(submit)) {
				highLighterMethod(submit);
				clickOn(submit);
			}
		} catch (Exception e) {
			testStepFailed("Could not click on Submit");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to check for all empty field errors together.
	 */
	public void checkEmptyFieldErrors() {
		try {
			if (isElementDisplayed(oneOrMoreFields_Error)) {
				highLighterMethod(oneOrMoreFields_Error);
				scrollToViewElement(oneOrMoreFields_Error);
				if (isElementDisplayed(department_EmptyError) && isElementDisplayed(firstName_EmptyError)
						&& isElementDisplayed(lastName_EmptyError) && isElementDisplayed(email_EmptyError)
						&& isElementDisplayed(phone_EmptyError)) {
					highLighterMethod(department_EmptyError);
					highLighterMethod(firstName_EmptyError);
					highLighterMethod(lastName_EmptyError);
					highLighterMethod(email_EmptyError);
					highLighterMethod(phone_EmptyError);
					testStepInfo("All Empty Field Errors were displayed");
				} else
					testStepFailed("All Empty Field Errors were not displayed");
			}
		} catch (Exception e) {
			testStepFailed("Could not validate empty field errors successfully");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to check for invalid email and invalid phone Errors.
	 */
	public void checkInvalidDataErrors() {
		try {
			if (isElementDisplayed(EmailAddressInvalidError)) {
				highLighterMethod(EmailAddressInvalidError);
				testStepInfo("Invalid Email Error was displayed");
			} else
				testStepFailed("Invalid Email Error was not displayed");
			if (isElementDisplayed(telephoneInvalidInvalidError)) {
				highLighterMethod(telephoneInvalidInvalidError);
				testStepInfo("Invalid Phone Number Error was displayed");
			} else
				testStepFailed("Invalid Phone Number Error was not displayed");
		} catch (Exception e) {
			testStepFailed("Could not validate Invalid email and Invalid phone nummber errors successfully");
			e.printStackTrace();
		}
	}

	/**
	 * Description: Method to verify presence of Success Message.
	 */
	public void verifySuccessMessage() {
		try {
			if (isElementDisplayed(successMessage)) {
				highLighterMethod(successMessage);
				testStepInfo("Success Message was displayed");
			} else
				testStepFailed("Success Message was not displayed");
		} catch (Exception e) {
			testStepFailed("Sucess Message could not be validated successfully");
			e.printStackTrace();
		}
	}

}
