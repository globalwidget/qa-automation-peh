package scenarios.contact;

import baseClass.BaseClass;
import iSAFE.ApplicationKeywords;
import iSAFE.GOR;
import pages.ContactUs;
import pages.HeaderAndFooters;
import pages.HomePage;

public class Contact extends ApplicationKeywords {
	BaseClass obj;
	HomePage homePage;
	HeaderAndFooters headerAndFooters;
	ContactUs contactUs;
	private boolean status = false;

	String password;

	public Contact(BaseClass obj) {
		super(obj);
		this.obj = obj;
		homePage = new HomePage(obj);
		headerAndFooters = new HeaderAndFooters(obj);
		contactUs = new ContactUs(obj);
	}

	/*
	 * TestCaseid : Contact Description : Verify proper error message is displayed
	 * for the invalid fields in the form
	 */
	public void submitInvalidDetails() {
		try {

			password = retrieve("securityPassword");

			String InvalidEmail = retrieve("invalidEmail");
			String Invalidphone = retrieve("invalidPhone");

			if (GOR.securityLogin == false) {
				homePage.securityLogin(password);
			}

			if (GOR.OfferPopUpHandled == false) {
				homePage.closeOfferPopup();
			}

			headerAndFooters.goTo_ContactUs();
			contactUs.fillDetails(" ", " ", " ", " ", " ");
			contactUs.clickSend();
			contactUs.checkEmptyFieldErrors();
			contactUs.fillDetails(" ", InvalidEmail, Invalidphone, " ", " ");
			contactUs.clickSend();
			contactUs.checkInvalidDataErrors();

		} catch (Exception e) {
			e.printStackTrace();
		}
		if (obj.testFailure || homePage.testFailure || headerAndFooters.testFailure || contactUs.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * TestCaseid : Contact Description :Verify if the user is able to submit the
	 * contact form
	 */
	public void submitValidDetails() {
		try {
			password = retrieve("securityPassword");
			String contactName_data = retrieve("contactName");
			String email_data = retrieve("email");
			String phone_data = retrieve("phone");
			String subject_data = retrieve("subject");
			String description_data = retrieve("description");

			if (GOR.securityLogin == false) {
				homePage.securityLogin(password);
			}

			if (GOR.OfferPopUpHandled == false) {
				homePage.closeOfferPopup();
			}

			headerAndFooters.goTo_ContactUs();
			contactUs.fillDetails(contactName_data, email_data, phone_data, subject_data, description_data);
			contactUs.clickSend();
			contactUs.verifySuccessMessage();

		} catch (Exception e) {
			e.printStackTrace();
		}
		if (obj.testFailure || homePage.testFailure || headerAndFooters.testFailure || contactUs.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

}
