package scenarios.headerAndFooter;

import baseClass.BaseClass;
import iSAFE.ApplicationKeywords;
import iSAFE.GOR;
import pages.Cart_AllProducts;
import pages.HomePage;
import pages.Login_Register;
import pages.MyAccount;
import pages.MyCart;

public class HeaderAndFooters extends ApplicationKeywords {
	BaseClass obj;
	Login_Register login_register;
	HomePage homePage;
	MyAccount myaccount;
	pages.HeaderAndFooters headerAndFooters;
	Cart_AllProducts cart_AllProducts;
	MyCart mycart;
	private boolean status = false;

	String password;

	public HeaderAndFooters(BaseClass obj) {
		super(obj);
		this.obj = obj;
		login_register = new Login_Register(obj);
		homePage = new HomePage(obj);
		myaccount = new MyAccount(obj);
		cart_AllProducts = new Cart_AllProducts(obj);
		headerAndFooters = new pages.HeaderAndFooters(obj);
		mycart = new MyCart(obj);
	}

	/*
	 * TestCaseid : General Description : Verify all the header menus are navigating
	 * to the respective pages
	 */
	public void verifyHeader() {
		try {
			password = retrieve("securityPassword");

			if (GOR.securityLogin == false) {
				homePage.securityLogin(password);
			}

			homePage.closeOfferPopup();

//			if (GOR.loggedIn == true) {
//				headerAndFooters.goToMyAccount();
//				myaccount.clickOnLogout();
//			}

			headerAndFooters.Verifypresence_PhoneNumber();
			headerAndFooters.goTo_CustomerService();
			headerAndFooters.verifyNavigation("contactUs");
			headerAndFooters.goTo_BecomeDistributor();
			headerAndFooters.verifyNavigation("becomeDistributor");
//			headerAndFooters.goToMyAccount();
//			login_register.login(retrieve("loginUsername"), retrieve("loginPassword"));
//			headerAndFooters.goToMyAccount();
//			myaccount.clickOnLogout();
//			headerAndFooters.clickOnMyCart();
//			headerAndFooters.verifyNavigation("cart");
//			mycart.removeAllProducts();
//			headerAndFooters.verifyNavigation("emptyCart");
//			headerAndFooters.goTo_Shop(0);
//			cart_AllProducts.selectFirstProductWithAddToCart();
//			headerAndFooters.clickOnCheckout();
//			headerAndFooters.verifyNavigation("checkout");
			headerAndFooters.clickFacebookIcon_Header();
			headerAndFooters.verifyNavigation("facebook");
			headerAndFooters.clickInstagramIcon_Header();
			headerAndFooters.verifyNavigation("instagram");
			headerAndFooters.clickTwitterIcon_Header();
			headerAndFooters.verifyNavigation("twitter");
			headerAndFooters.goTo_Home();
			headerAndFooters.verifyNavigation("homeBanner");
			headerAndFooters.goTo_Shop(0);
			headerAndFooters.verifyNavigation("shop");
			headerAndFooters.goTo_Shop(1);
			headerAndFooters.verifyNavigation("CBDdogChews");
			headerAndFooters.goTo_Shop(2);
			headerAndFooters.verifyNavigation("CBDoilForCats");
			headerAndFooters.goTo_Shop(3);
			headerAndFooters.verifyNavigation("cbdOilForPets");
			headerAndFooters.goTo_Shop(4);
			headerAndFooters.verifyNavigation("CBDPawButter");
			headerAndFooters.goTo_Shop(5);
			headerAndFooters.verifyNavigation("CBDshampooConditioner");
			headerAndFooters.goTo_Shop(6);
			headerAndFooters.verifyNavigation("CBDbundle");
			headerAndFooters.goTo_Shop(7);
			headerAndFooters.verifyNavigation("shop");

			headerAndFooters.goTo_AboutUs();
			headerAndFooters.verifyNavigation("aboutUs");

			headerAndFooters.goTo_Learn(0);
			headerAndFooters.verifyNavigation("cbdBenefitsPets");
			headerAndFooters.goTo_Learn(1);
			headerAndFooters.verifyNavigation("petBlog");
			headerAndFooters.goTo_Learn(2);
			headerAndFooters.verifyNavigation("cbdBenefitsPets");
			headerAndFooters.goTo_Learn(3);
			headerAndFooters.verifyNavigation("labTesting");
			headerAndFooters.goTo_Learn(4);
			headerAndFooters.verifyNavigation("intheNews");
			headerAndFooters.goTo_Learn(5);
			headerAndFooters.verifyNavigation("faq");
			headerAndFooters.goTo_Learn(6);
			headerAndFooters.verifyNavigation("petDosing");

			headerAndFooters.goTo_ContactUs();
			headerAndFooters.verifyNavigation("contactUs");

//			headerAndFooters.Verify_PrivacyPolicy();

		} catch (Exception e) {
			testStepFailed("Header Navigation could not be verified");
			e.printStackTrace();
		}
		if (obj.testFailure || homePage.testFailure || login_register.testFailure || headerAndFooters.testFailure
				|| mycart.testFailure || myaccount.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * TestCaseid : General Description : Verify all the footer menus are navigating
	 * to the respective pages
	 */
	public void verifyFooter() {
		try {
			password = retrieve("securityPassword");

			if (GOR.securityLogin == false) {
				homePage.securityLogin(password);
			}

			homePage.closeOfferPopup();

			headerAndFooters.navigateFooter("company");
			headerAndFooters.verifyNavigation("aboutUs");
			headerAndFooters.navigateFooter("labResults");
			headerAndFooters.verifyNavigation("labTesting");
			headerAndFooters.navigateFooter("wholesale");
			headerAndFooters.verifyNavigation("becomeDistributor");
//			headerAndFooters.navigateFooter("myAccount");
//			headerAndFooters.verifyNavigation("myAccount");
			headerAndFooters.navigateFooter("cbdProducts");
			headerAndFooters.verifyNavigation("shop");
//			headerAndFooters.navigateFooter("myCart");
//			headerAndFooters.verifyNavigation("cart");
//			headerAndFooters.goTo_Shop(0);
//			cart_AllProducts.selectFirstProductWithAddToCart();
//			headerAndFooters.navigateFooter("checkout");
//			headerAndFooters.verifyNavigation("checkout");
			headerAndFooters.navigateFooter("contact");
			headerAndFooters.verifyNavigation("contactUs");
			headerAndFooters.navigateFooter("ccpa");
			headerAndFooters.verifyNavigation("ccpa");
//			headerAndFooters.navigateFooter("shippingPolicy");
//			headerAndFooters.verifyNavigation("shippingPolicy");
//			headerAndFooters.navigateFooter("refundPolicy");
//			headerAndFooters.verifyNavigation("refundPolicy");
//			headerAndFooters.navigateFooter("newsletter");
//			headerAndFooters.verifyNavigation("newsletter");
//			headerAndFooters.navigateFooter("seniorsProgram");
//			headerAndFooters.verifyNavigation("seniorsProgram");
//			headerAndFooters.navigateFooter("veteransProgram");
//			headerAndFooters.verifyNavigation("veteransProgram");
//			headerAndFooters.navigateFooter("firstRespondersProgram");
//			headerAndFooters.verifyNavigation("firstResponders");
			headerAndFooters.navigateFooter("facebook");
			headerAndFooters.verifyNavigation("facebookFooter");
			headerAndFooters.navigateFooter("instagram");
			headerAndFooters.verifyNavigation("instagramFooter");
			headerAndFooters.navigateFooter("twitter");
			headerAndFooters.verifyNavigation("twitterFooter");

		} catch (Exception e) {
			testStepFailed("Footer Navigation could not be verified");
			e.printStackTrace();
		}
		if (obj.testFailure || homePage.testFailure || login_register.testFailure || headerAndFooters.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

}
