package scenarios.cart;

import baseClass.BaseClass;
import iSAFE.ApplicationKeywords;
import iSAFE.GOR;
import pages.Cart_AllProducts;
import pages.HeaderAndFooters;
import pages.HomePage;
import pages.Login_Register;
import pages.MyAccount;
import pages.MyCart;

public class Cart extends ApplicationKeywords {
	BaseClass obj;
	Login_Register login_register;
	HomePage homePage;
	MyAccount myaccount;
	Cart_AllProducts cart_AllProducts;
	MyCart myCart;
	HeaderAndFooters headerAndfooter;
	private boolean status = false;

	String password;

	public Cart(BaseClass obj) {
		super(obj);
		this.obj = obj;
		login_register = new Login_Register(obj);
		homePage = new HomePage(obj);
		myaccount = new MyAccount(obj);
		cart_AllProducts = new Cart_AllProducts(obj);
		myCart = new MyCart(obj);
		headerAndfooter = new HeaderAndFooters(obj);
	}

	/*
	 * TestCaseid : Cart and PGP Description : To add a product to the cart.
	 */
	public void addProductToCart(int numberOfproducts, boolean addOneProduct_PGP) {
		try {
			password = retrieve("securityPassword");
			String Username = retrieve("loginUsername");
			String Password = retrieve("loginPassword");
			String productName = null;
			String secondProductName = null;

			if (GOR.securityLogin == false) {
				homePage.securityLogin(password);
			}

			if (GOR.OfferPopUpHandled == false) {
				homePage.closeOfferPopup();
			}

			if (GOR.loggedIn == false) {
				headerAndfooter.goToMyAccount();
				login_register.login(Username, Password);
			}
			headerAndfooter.clickOnMyCart();
			myCart.removeAllProducts();
			headerAndfooter.goTo_Shop(0);
			if (addOneProduct_PGP == true) {

				productName = cart_AllProducts.selectFirstProductWithAddToCart();

			} else {
				cart_AllProducts.selectFirstProduct();
				productName = cart_AllProducts.getProductTitle();
				cart_AllProducts.chooseFlavorAndStrength();
				cart_AllProducts.clickAddToCart();
				if (numberOfproducts == 2) {
					headerAndfooter.goTo_Shop(0);
					cart_AllProducts.selectSecondProduct();
					secondProductName = cart_AllProducts.getProductTitle();
					cart_AllProducts.chooseFlavorAndStrength();
					cart_AllProducts.clickAddToCart();

				}
			}
			cart_AllProducts.clickViewCart();
			myCart.verifyPresenceOfExpectedProduct(productName);
			if (numberOfproducts == 2) {
				myCart.verifyPresenceOfExpectedProduct(secondProductName);
			}

		} catch (Exception e) {
			testStepFailed("Add To Cart could not be done");
		}
		if (obj.testFailure || homePage.testFailure || login_register.testFailure || cart_AllProducts.testFailure
				|| myCart.testFailure || headerAndfooter.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * TestCaseid : Cart Description : To validate removing a product to the cart.
	 */
	public void verifyRemoveProductFromCart() {
		try {
			password = retrieve("securityPassword");
			String Username = retrieve("loginUsername");
			String Password = retrieve("loginPassword");
			String productRemoved;

			if (GOR.securityLogin == false) {
				homePage.securityLogin(password);
			}

			if (GOR.OfferPopUpHandled == false) {
				homePage.closeOfferPopup();
			}

			if (GOR.loggedIn == false) {
				headerAndfooter.goToMyAccount();
				login_register.login(Username, Password);
			}
			if (GOR.productAdded == false) {
				headerAndfooter.goTo_Shop(0);
				cart_AllProducts.selectFirstProduct();
				cart_AllProducts.chooseFlavorAndStrength();
				cart_AllProducts.clickAddToCart();
				cart_AllProducts.clickViewCart();
			} else {
				headerAndfooter.clickOnMyCart();
			}
			productRemoved = myCart.clickOnRemoveProduct();
			myCart.verifyExpectedMessageAlert(productRemoved.split("-")[0].trim(), "removed", "");

		} catch (Exception e) {
			testStepFailed("Removal of product could not be verified");
		}
		if (obj.testFailure || homePage.testFailure || login_register.testFailure || cart_AllProducts.testFailure
				|| myCart.testFailure || headerAndfooter.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * TestCaseid : Cart Description : Verify if the user is able to update the
	 * quantity of the products in the cart page
	 */
	public void verifyUpdateQuantityInCart() {
		try {
			password = retrieve("securityPassword");
			String Username = retrieve("loginUsername");
			String Password = retrieve("loginPassword");

			if (GOR.securityLogin == false) {
				homePage.securityLogin(password);
			}

			if (GOR.OfferPopUpHandled == false) {
				homePage.closeOfferPopup();
			}

			if (GOR.loggedIn == false) {
				headerAndfooter.goToMyAccount();
				login_register.login(Username, Password);
			}
			if (GOR.productAdded == false) {
				headerAndfooter.goTo_Shop(0);
				cart_AllProducts.selectFirstProduct();
				cart_AllProducts.chooseFlavorAndStrength();
				cart_AllProducts.clickAddToCart();
				cart_AllProducts.clickViewCart();
			} else {
				headerAndfooter.clickOnMyCart();
			}
			scrollUp();
			myCart.updateCartQuantity(10);
			myCart.clickUpdateCart();
			myCart.verifyExpectedMessageAlert("Cart updated", "", "");
		} catch (Exception e) {
			testStepFailed("Update Quantity of cart for a given product in the cart page could not be verified");
		}
		if (obj.testFailure || homePage.testFailure || login_register.testFailure || cart_AllProducts.testFailure
				|| myCart.testFailure || headerAndfooter.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * TestCaseid : Cart Description : Verify if the user is able to apply the valid
	 * coupon code in the cart page
	 */
	public void verifyValidCouponEntry() {
		try {
			password = retrieve("securityPassword");
			String Username = retrieve("loginUsername");
			String Password = retrieve("loginPassword");
			String validCouponCode = retrieve("validCouponCode");

			if (GOR.securityLogin == false) {
				homePage.securityLogin(password);
			}

			if (GOR.OfferPopUpHandled == false) {
				homePage.closeOfferPopup();
			}

			if (GOR.loggedIn == false) {
				headerAndfooter.goToMyAccount();
				login_register.login(Username, Password);
			}
			headerAndfooter.clickOnMyCart();
			headerAndfooter.goTo_Shop(0);
			cart_AllProducts.selectFirstProduct();
			cart_AllProducts.chooseFlavorAndStrength();
			cart_AllProducts.clickAddToCart();
			cart_AllProducts.clickViewCart();
			myCart.enterCouponCode(validCouponCode);
			myCart.clickApplyCoupon();
			myCart.verifyExpectedMessageAlert("Coupon code applied successfully", "", "");
			myCart.removeCoupon();
		} catch (Exception e) {
			testStepFailed("Valid Coupon Entry could not be verified");
		}
		if (obj.testFailure || homePage.testFailure || login_register.testFailure || cart_AllProducts.testFailure
				|| myCart.testFailure || headerAndfooter.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * TestCaseid : Cart Description : Verify if the user is able to apply the valid
	 * coupon code in the cart page
	 */
	public void verifyInvalidCouponEntry() {
		try {
			password = retrieve("securityPassword");
			String Username = retrieve("loginUsername");
			String Password = retrieve("loginPassword");
			String invalidCouponCode = retrieve("invalidCouponCode");

			if (GOR.securityLogin == false) {
				homePage.securityLogin(password);
			}

			if (GOR.OfferPopUpHandled == false) {
				homePage.closeOfferPopup();
			}

			if (GOR.loggedIn == false) {
				headerAndfooter.goToMyAccount();
				login_register.login(Username, Password);
			}
			if (GOR.productAdded == false) {
				headerAndfooter.goTo_Shop(0);
				cart_AllProducts.selectFirstProduct();
				cart_AllProducts.chooseFlavorAndStrength();
				cart_AllProducts.clickAddToCart();
				cart_AllProducts.clickViewCart();
			} else {
				headerAndfooter.clickOnMyCart();
			}
			myCart.enterCouponCode(invalidCouponCode);
			myCart.clickApplyCoupon();
			myCart.verifyExpectedMessageAlert_InvalidCoupon("Coupon", "does not exist", invalidCouponCode);
		} catch (Exception e) {
			testStepFailed("Invalid Coupon Entry could not be verified");
		}
		if (obj.testFailure || homePage.testFailure || login_register.testFailure || cart_AllProducts.testFailure
				|| myCart.testFailure || headerAndfooter.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * TestCaseid : PDP Description : Verify if the user is able to modify the
	 * quantity of the product and add to cart
	 */
	public void verifyUpdateQuantity_AddToCart() {
		try {
			password = retrieve("securityPassword");
			String Username = retrieve("loginUsername");
			String Password = retrieve("loginPassword");

			if (GOR.securityLogin == false) {
				homePage.securityLogin(password);
			}

			if (GOR.OfferPopUpHandled == false) {
				homePage.closeOfferPopup();
			}

			if (GOR.loggedIn == false) {
				headerAndfooter.goToMyAccount();
				login_register.login(Username, Password);
			}
			if (GOR.productAdded == true) {
				headerAndfooter.clickOnMyCart();
				myCart.removeAllProducts();
			}
			headerAndfooter.goTo_Shop(0);
			cart_AllProducts.selectFirstProduct();
			cart_AllProducts.chooseFlavorAndStrength();
			myCart.updateCartQuantity(10);
			cart_AllProducts.clickAddToCart();
			cart_AllProducts.clickViewCart();
			myCart.updateCartQuantity(5);
		} catch (Exception e) {
			testStepFailed("Update Quantity of a product in PDP page could not be verified");
		}
		if (obj.testFailure || homePage.testFailure || login_register.testFailure || cart_AllProducts.testFailure
				|| myCart.testFailure || headerAndfooter.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}
}
