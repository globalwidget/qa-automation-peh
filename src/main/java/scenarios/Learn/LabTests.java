package scenarios.Learn;

import baseClass.BaseClass;
import iSAFE.ApplicationKeywords;
import iSAFE.GOR;
import pages.HeaderAndFooters;
import pages.HomePage;

public class LabTests extends ApplicationKeywords {
	BaseClass obj;
	pages.LabTestingResults labTests;
	HomePage homePage;
	HeaderAndFooters headerAndFooter;
	private boolean status = false;

	String password;

	public LabTests(BaseClass obj) {
		super(obj);
		this.obj = obj;
		homePage = new HomePage(obj);
		labTests = new pages.LabTestingResults(obj);
		headerAndFooter = new HeaderAndFooters(obj);
	}

	/*
	 * TestCaseid : Lab Test - Batch Search Description : Verify proper error
	 * message is displayed for the invalid batch search
	 */
	public void verifyInvalidSearch() {
		try {
			password = retrieve("securityPassword");
			if (GOR.securityLogin == false) {
				homePage.securityLogin(password);
			}

			if (GOR.OfferPopUpHandled == false) {
				homePage.closeOfferPopup();
			}

			headerAndFooter.goTo_Learn(3);
			labTests.clickSearchIcon();
			labTests.enterBatchNumber("  ");
			labTests.clickSearchIcon();
			labTests.verifyNullResult();
		} catch (Exception e) {
			testStepFailed("Error message for invalid batch search data could not be verified");
		}
		if (obj.testFailure || homePage.testFailure || labTests.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * TestCaseid : Lab Test - Batch Search Description : Verify if the user is able
	 * to search for lab test results using the valid batch number
	 */
	public void verifyValidSearch() {
		try {
			password = retrieve("securityPassword");
			String batchNumber = retrieve("batchNumber");

			if (GOR.securityLogin == false) {
				homePage.securityLogin(password);
			}

			if (GOR.OfferPopUpHandled == false) {
				homePage.closeOfferPopup();
			}

			headerAndFooter.goTo_Learn(3);
			labTests.enterBatchNumber(batchNumber);
			labTests.clickSearchIcon();
			labTests.checkViewResults();
		} catch (Exception e) {
			testStepFailed("Valid search for labtest results could not be verified");
		}
		if (obj.testFailure || homePage.testFailure || labTests.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}
}