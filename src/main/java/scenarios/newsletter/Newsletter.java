package scenarios.newsletter;

import baseClass.BaseClass;
import iSAFE.ApplicationKeywords;
import iSAFE.GOR;
import pages.HeaderAndFooters;
import pages.HomePage;

public class Newsletter extends ApplicationKeywords {
	BaseClass obj;
	HomePage homePage;
	HeaderAndFooters headersAndFooters;
	pages.Newsletter newsletter;
	private boolean status = false;

	String password;

	public Newsletter(BaseClass obj) {
		super(obj);
		this.obj = obj;
		homePage = new HomePage(obj);
		newsletter = new pages.Newsletter(obj);
		headersAndFooters = new HeaderAndFooters(obj);
	}

	/*
	 * TestCaseid : Newsletter Description : Verify the empty field errors and the
	 * error message for invalid email address.
	 */
	public void newsletter_invalidData() {
		try {
			password = retrieve("securityPassword");
			String invalidEmail = retrieve("invalidEmail");

			if (GOR.securityLogin == false) {
				homePage.securityLogin(password);
			}
			
			if (GOR.OfferPopUpHandled == false) {
				homePage.closeOfferPopup();
			}
			headersAndFooters.goTo_Home();
			newsletter.clickSignAndSave();
			newsletter.verifyEmptyFieldErrors();
			newsletter.enterDetails(invalidEmail, "", "");
			newsletter.clickSignAndSave();
			newsletter.verifyInvalidEmailErrorNewsletter();

		} catch (Exception e) {
			e.printStackTrace();
		}
		if (obj.testFailure || homePage.testFailure || headersAndFooters.testFailure || newsletter.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * TestCaseid : Newsletter Description : Verify if the user is able to sign up
	 * for newsletter with valid details
	 */
	public void newsletter_validData() {
		try {
			password = retrieve("securityPassword");
			String email = retrieve("email");
			String birthday = retrieve("birthday");

			if (GOR.securityLogin == false) {
				homePage.securityLogin(password);
			}
			
			if (GOR.OfferPopUpHandled == false) {
				homePage.closeOfferPopup();
			}

			headersAndFooters.goTo_Home();
			newsletter.enterDetails(email, " ", birthday);
			newsletter.clickSignAndSave();
			newsletter.verifySuccessMessage();
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (obj.testFailure || homePage.testFailure || headersAndFooters.testFailure || newsletter.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

}
