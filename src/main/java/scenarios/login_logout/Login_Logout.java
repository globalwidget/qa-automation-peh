package scenarios.login_logout;

import baseClass.BaseClass;
import iSAFE.ApplicationKeywords;
import iSAFE.GOR;
import pages.HeaderAndFooters;
import pages.HomePage;
import pages.Login_Register;
import pages.MyAccount;

public class Login_Logout extends ApplicationKeywords {
	BaseClass obj;
	Login_Register login_register;
	HomePage homePage;
	MyAccount myaccount;
	HeaderAndFooters headerAndfooter;
	private boolean status = false;

	String password;

	public Login_Logout(BaseClass obj) {
		super(obj);
		this.obj = obj;
		login_register = new Login_Register(obj);
		homePage = new HomePage(obj);
		myaccount = new MyAccount(obj);
		headerAndfooter = new HeaderAndFooters(obj);
	}

	/*
	 * TestCaseid : Login Description : To validate login with invalid credentials.
	 */
	public void login_invalidCredentials() {
		try {
			password = retrieve("securityPassword");
			String invalidUsername = retrieve("invalidUsername");
			String invalidPassword = retrieve("invalidPassword");

			if (GOR.securityLogin == false) {
				homePage.securityLogin(password);
			}

			if (GOR.OfferPopUpHandled == false) {
				homePage.closeOfferPopup();
			}

			headerAndfooter.goToMyAccount();
			login_register.login(" ", " ");
			login_register.verifyErrorMessage_Login("usernameEmpty");
			login_register.login(invalidUsername, "  ");
			login_register.verifyErrorMessage_Login("passwordEmpty");
			login_register.login(invalidUsername, invalidPassword);
			login_register.verifyErrorMessage_Login("invalidCredentials");

		} catch (Exception e) {
			testStepFailed("Error message for Login using Invalid Credentials could not be verified");
		}
		if (obj.testFailure || homePage.testFailure || login_register.testFailure || headerAndfooter.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * TestCaseid : Login Description : To validate login with valid credentials
	 */

	public void login() {
		try {
			password = retrieve("securityPassword");
			String loginUsername = retrieve("loginUsername");
			String loginPassword = retrieve("loginPassword");

			if (GOR.securityLogin == false) {
				homePage.securityLogin(password);
			}

			if (GOR.OfferPopUpHandled == false) {
				homePage.closeOfferPopup();
			}

			headerAndfooter.goToMyAccount();
			login_register.login(loginUsername, loginPassword);
			myaccount.verifyPresenceOfLogout();
			myaccount.clickOnLogout();
		} catch (Exception e) {
			testStepFailed("Login using valid Credentials could not be verified");
		}
		if (obj.testFailure || homePage.testFailure || login_register.testFailure || myaccount.testFailure
				|| headerAndfooter.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

	/*
	 * TestCaseid : Login Description : To validate logout.
	 */
	public void logout() {
		try {
			password = retrieve("securityPassword");
			String loginUsername = retrieve("loginUsername");
			String loginPassword = retrieve("loginPassword");

			if (GOR.securityLogin == false) {
				homePage.securityLogin(password);
			}

			if (GOR.OfferPopUpHandled == false) {
				homePage.closeOfferPopup();
			}

			headerAndfooter.goToMyAccount();
			login_register.login(loginUsername, loginPassword);
			myaccount.clickOnLogout();
			headerAndfooter.goToMyAccount();
			login_register.verifyPresenceOfLogin();
		} catch (Exception e) {
			testStepFailed("Logout could not be done");
		}
		if (obj.testFailure || homePage.testFailure || login_register.testFailure || myaccount.testFailure) {
			status = true;
		}
		this.testFailure = status;
	}

}
